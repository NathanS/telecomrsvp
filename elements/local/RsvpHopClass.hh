#ifndef RSVPHOPCLASS_H
#define RSVPHOPCLASS_H

struct RsvpHopClass {
	in_addr hopAdress; //Can be prev or next
	uint16_t logicalInterfaceHandle;
};

#endif
