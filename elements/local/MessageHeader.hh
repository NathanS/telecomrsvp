#ifndef MESSAGEHEADER_H
#define MESSAGEHEADER_H


struct MessageHeader {
	//unsigned int V : 4; //Version Number
	//unsigned int unused : 12;
	uint16_t Vunused;
	uint16_t overallLength; //In 32 Bit Words
};

struct PerServiceDataHeader {
	uint8_t svcNumber;
	//unsigned int B : 1; //Break bit, unused.
	//unsigned int reserved : 7;
	uint8_t Breserved;
	uint16_t scvLength;
};

struct ParameterHeader {
	uint8_t paramNum;
	uint8_t flags;
	uint16_t paramLength;
};

#endif
