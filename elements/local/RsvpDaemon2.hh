#ifndef RSVPDAEMON2_HH_
#define RSVPDAEMON2_HH_
#include <click/element.hh>
#include <click/timer.hh>
#include <click/vector.hh>
#include <clicknet/ip.h>
#include "PathState.hh"
#define MAX_UINT16_VALUE 65535
/*
 * This is the RSVP Daemon element that handles all RSVP related traffic.
 * @input: Only input source.
 * @output[0] Output 0 transmits all (Newly Generated) RSVP Packets.
 * @output[1] Output 1 transmits all packets that receive QOS. (Have a session defined within the element.
 * @output[2] Output 2 transmits all packets that do not match the session, and are given lower priority.
 * @output[3] Output 3 transmits all RSVP Packets that were modified inside the element, but not generated.
 * 		This is done to create an abstraction between stripping inside the click element.
 */
CLICK_DECLS
class RsvpDaemon2 : public Element
{
public:
	RsvpDaemon2();
	~RsvpDaemon2();
	const char *class_name() const	{ return "RsvpDaemon2"; }
	const char *port_count() const 	{ return "1/4"; }
	const char *processing() const	{ return PUSH; }

	int initialize(ErrorHandler*errh);
	int configure(Vector<String>&, ErrorHandler*);
	Packet * GenPath(IPAddress s, uint16_t sp, IPAddress d, uint16_t dp);
	Packet * GenResv(IPAddress DST, uint16_t DSTPORT, uint8_t proto, uint16_t srcport, IPAddress srcaddr);
	Packet * GenPathTear();
	Packet * GenResvTear();
	Packet * GenPathErr(uint8_t ErrorCode, uint16_t ErrorValue);
	Packet * GenResvErr(uint8_t ErrorCode, uint16_t ErrorValue);
	Packet * GenResvConf(IPAddress);
	void InitRsvp(IPAddress s,IPAddress d, uint16_t sp, uint16_t dp);
	void SenderInitTearDown();
	void ReceiverInitTearDown();//
	Packet * IpEncap(Packet* ,int, IPAddress, IPAddress, bool RAP);
	bool CleanupPathState(SessionClass, RsvpSenderTemplate);


	void push(int, Packet *);
	void run_timer(Timer* t);
	//void add_handlers();

private:
	IPAddress m_localIP;
	IPAddress m_RSVP_Dst;
	Timer m_PathInit_timer;
	bool m_first_time;
	bool m_RSVP_active;
	bool m_RSVP_Sender;
	bool m_RSVP_Receiver;
	bool m_give_prio;
	bool m_rsvp_started;
	bool m_rsvp_setup_complete;
	bool m_verbose;
	bool m_periodic_path_sending;
	bool m_Receiver_ResvTear_init;
	int m_sender_period_count;
	double m_local_state_lifetime;
	uint32_t R;

	PathState m_path_state;
	IPAddress m_next_hop;
	bool m_path_set;
	bool m_resv_set;
	Vector<PathState> PsVector;

	IPAddress m_packet_source_address;
	uint16_t m_packet_source_port;
	IPAddress m_packet_dst_address;
	uint16_t m_packet_dst_port;
	uint16_t m_ip_id_count;

	//Will define the session for the sender.
	uint16_t m_prio_port;
	IPAddress m_Sender_Session_DST;
	uint8_t m_Sender_Session_Protocol;
	uint16_t m_Sender_Session_DSTPORT;

	bool m_Sender_init_breakoff;
	bool m_Receiver_init_breakoff;


};

CLICK_ENDDECLS
#endif /* RSVPDAEMON2_HH_ */
