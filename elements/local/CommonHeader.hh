#ifndef COMMONHEADER_H
#define COMMONHEADER_H

#include <stdint.h>

struct CommonHeader {
	uint8_t verflags;
	uint8_t msg_type;
	int16_t checksum;
	uint8_t send_ttl;
	uint8_t reserved; 
	uint16_t Rsvp_length;
	//See RFC 2205 Page 32
};

#endif
