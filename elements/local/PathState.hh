/*
 * PathState.hh
 *
 *  Created on: Aug 8, 2016
 *      Author: paul
 */

#ifndef PATHSTATE_HH_
#define PATHSTATE_HH_

#include <stdint.h>
#include "RsvpSenderTSpec.hh"
#include "RsvpSenderTemplate.hh"
#include "SessionClass.hh"

struct PathState {
	/*
Each RSVP-capable node along the path(s) captures a Path
message and processes it to create path state for the sender
defined by the SENDER_TEMPLATE and SESSION objects. Any
POLICY_DATA, SENDER_TSPEC, and ADSPEC objects are also saved in
the path state. If an error is encountered while processing a
Path message, a PathErr message is sent to the originating
sender of the Path message. Path messages must satisfy the
rules on SrcPort and DstPort in Section 3.2.
	 */
	IPAddress prev_hop_node;//the unicast IP address of the previous hop node
	IPAddress next_hop_node;
	RsvpSenderTSpec ts;//Page 37
	RsvpSenderTemplate stemplate;
	SessionClass session;
	uint32_t L;
	bool path_state_set;
	bool resv_state_set;
};



#endif /* PATHSTATE_HH_ */
