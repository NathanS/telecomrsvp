
#ifndef SETTOSBYTE_HH_
#define SETTOSBYTE_HH_

#include <click/element.hh>
#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>

CLICK_DECLS

class setTOSByte : public Element
{
public:
	setTOSByte();
	~setTOSByte();

	const char *class_name() const	{ return "setTOSByte"; }
	const char *port_count() const 	{ return "1/1"; }
	const char *processing() const	{ return PUSH; }

	int initialize(ErrorHandler*errh);
	int configure(Vector<String>&, ErrorHandler*);

	void push(int, Packet *);
private:
	int m_value;
};

CLICK_ENDDECLS
#endif /* SETTOSBYTE_HH_ */
