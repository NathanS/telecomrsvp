#ifndef RSVPSENDERTSPEC_H
#define RSVPSENDERTSPEC_H

struct RsvpSenderTSpec{
	uint16_t msgformvnumreserved;
	uint16_t overallLength;
	uint8_t serviceNumber;

	uint8_t nreserved2;

	uint16_t lengthOfServiceData;
	uint8_t parameterId;

	uint8_t flags;
	uint16_t fin;

	uint32_t r; //Token Bucket Rate
	uint32_t b; //Token Bucket Size
	uint32_t p; //Peak data rate
	int32_t m; //Minimum Policed Unit
	int32_t M; //Maximum Packet Size
};

#endif
