#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>

#include "setTOSByte.hh"

CLICK_DECLS

setTOSByte::setTOSByte()
{
	// TODO Auto-generated constructor stub

}

setTOSByte::~setTOSByte()
{
	// TODO Auto-generated destructor stub
}

int setTOSByte::initialize(ErrorHandler* errh)
{

}

int setTOSByte::configure(Vector<String>& conf, ErrorHandler* errh)
{
	if (cp_va_kparse(conf, this, errh,"TOSVALUE", cpkM, cpInteger, &m_value, cpEnd) < 0) {
		return -1;
	}
	return 0;
}

void setTOSByte::push(int int1, Packet* p)
{
	//assume the packet we receive is a valid IP packet.
	WritablePacket *q = p->uniqueify();
	click_ip *my_header = q->ip_header();

	click_chatter("Old value for TOS Byte:%i", my_header->ip_tos);
	//my_header->ip_tos = (uint8_t)m_value;
	my_header->ip_tos = m_value;
	click_chatter("New value for TOS byte:%i", my_header->ip_tos);

	my_header->ip_sum = 0;
	click_chatter("Value of IP HL: %d, with size %d", my_header->ip_hl, 4);
	int l = my_header->ip_hl << 2; //TODO: Find out why this happens

	click_chatter("Value of IP HL, after shifting: %d", my_header->ip_hl << 2);
	//my_header->ip_sum = 0;
	my_header->ip_sum = click_in_cksum((unsigned char*) my_header,my_header->ip_hl << 2 );

	output(0).push(q);

}

CLICK_ENDDECLS
EXPORT_ELEMENT(setTOSByte)
