#ifndef RSVPSENDERTEMPLATE_H
#define RSVPSENDERTEMPLATE_H

#include <stdint.h>

struct RsvpSenderTemplate {
	//Same as Filter Spec!
	in_addr srcAdress; //Can be prev or next
	uint16_t voidbits;
	uint16_t srcPort;
};

#endif
