#ifndef SESSIONCLASS_H
#define	SESSIONCLASS_H

struct SessionClass {
	in_addr dstAddress;// 32
	uint8_t protocolId; // 8
	uint8_t flags; // 8
	uint16_t dstPort; //16
};
#endif
