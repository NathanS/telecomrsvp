#ifndef FILTERSPEC_H
#define FILTERSPEC_H

struct FilterSpec {
	in_addr srcAdress; //Can be prev or next
	uint16_t voidbits;
	uint16_t srcPort;
};

#endif
