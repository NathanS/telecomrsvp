#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/udp.h>
#include <clicknet/ether.h>
#include <clicknet/ip.h>
#include <elements/ip/ipencap.hh>
#include <arpa/inet.h>

#include "RsvpDaemon2.hh"
#include "RsvpSenderTemplate.hh"
#include "RsvpSenderTSpec.hh"
#include "CommonHeader.hh"
#include "ObjectHeader.hh"
#include "ObjectHeader.hh"
#include "SessionClass.hh"
#include "RsvpHopClass.hh"
#include "TimeValues.hh"
#include "Style.hh"
#include "FilterSpec.hh"
#include "RsvpFlowSpec.hh"
#include "ErrorSpec.hh"
#include "ResvConfirm.hh"
#include "RouterAlertIP.hh"

#define PATH_SIZE sizeof(CommonHeader) + 5*sizeof(ObjectHeader) + sizeof(SessionClass)+ sizeof(RsvpHopClass) + sizeof(TimeValues) + sizeof(RsvpSenderTemplate) + sizeof(RsvpSenderTSpec)
#define RESV_SIZE sizeof(CommonHeader) + 6*sizeof(ObjectHeader) + sizeof(SessionClass)+ sizeof(RsvpHopClass) + sizeof(TimeValues) + sizeof(Style) + sizeof(RsvpFlowSpec) + sizeof(FilterSpec)
#define RESV_TEAR_SIZE sizeof(CommonHeader) + 4 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass) + sizeof(Style) + sizeof(FilterSpec)
#define PATH_ERR_SIZE sizeof(CommonHeader) + 2 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(ErrorSpec)
#define RESV_ERR_SIZE sizeof(CommonHeader) + 4 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass) + sizeof(ErrorSpec) + sizeof(Style)
#define RESV_CONF_SIZE sizeof(CommonHeader) + 5 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(ErrorSpec)+ sizeof(ResvConfirm) + sizeof(Style) + sizeof(FilterSpec)
CLICK_DECLS
uint16_t RsvpChecksum2(unsigned char * message, int msg_size);

RsvpDaemon2::RsvpDaemon2()
	: m_PathInit_timer(this)
{
	// TODO Auto-generated constructor stub

}

RsvpDaemon2::~RsvpDaemon2()
{
	// TODO Auto-generated destructor stub
}

int RsvpDaemon2::initialize(ErrorHandler* errh)
{
	return 0;
}

bool RsvpDaemon2::CleanupPathState(SessionClass s, RsvpSenderTemplate t)
{
	Vector<PathState>::iterator del = 0;
	for (Vector<PathState>::iterator it = PsVector.begin(); it != PsVector.end(); it++) {
		if ((*it).session.dstAddress == s.dstAddress && (*it).session.dstPort == s.dstPort
		        && (*it).session.protocolId == s.protocolId && (*it).stemplate.srcAdress == t.srcAdress
		        && (*it).stemplate.srcPort == t.srcPort) {
			del = it;
			click_chatter("Found a state to delete.");
		}
	}
	if (del != 0) {
		PsVector.erase(del);
	} else {
		return false;
	}
	memset(&this->m_path_state, 0, sizeof(PathState));
	this->m_path_set = false;
	return true;
}
int RsvpDaemon2::configure(Vector<String>& conf, ErrorHandler* errh)
{
	//RSVP should know its own address, whether it should start up RSVP or not,
	if (cp_va_kparse(conf, this, errh, "ADDRESS", cpkM, cpIPAddress, &m_localIP, "PRIOPORT", cpkN, cpInteger,
	        &m_prio_port, "VERBOSE", cpkN, cpBool, &m_verbose, "DESTINATION", cpkN, cpIPAddress,
	        &m_Sender_Session_DST, "PROTOCOL", cpkN, cpInteger, &m_Sender_Session_Protocol, cpEnd) < 0) {
		return -1;
	}

	m_RSVP_Sender = false;
	m_RSVP_Receiver = false;
	m_first_time = true;
	m_give_prio = false;
	m_rsvp_started = false;
	m_Sender_init_breakoff = false;
	m_Receiver_init_breakoff = false;
	m_path_set = false;
	m_resv_set = false;
	m_periodic_path_sending = false;

	m_path_state.path_state_set = false;
	m_path_state.prev_hop_node = 0;

	m_ip_id_count = 1; //For IP Id field.
	m_sender_period_count = 0;

	m_PathInit_timer.initialize(this);
	m_PathInit_timer.schedule_after_msec(5000); //Timer for initial PATH messages by active sender
	return 0;
}

Packet * RsvpDaemon2::GenPath(IPAddress SRC, uint16_t SRCPORT, IPAddress DST, uint16_t DSTPORT)
{
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 5 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass)
	        + sizeof(TimeValues) + sizeof(RsvpSenderTemplate) + sizeof(RsvpSenderTSpec);
	int headroom = 0;
	//int headroom = 0;//No need for header space, we add it in the click script.
	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());
	//Fill in the common header
	CommonHeader* head = (CommonHeader*) (packet->data());

	head->verflags |= 1 << 4; //Version is 1, Flags are 0;
	head->msg_type = 1; // 1 = Path
	head->checksum = 0; //0 for calculation purpose, fill in at end
	head->send_ttl = 250; //Should be the same as the IP TTL field.
	head->reserved = 0;
	head->Rsvp_length = htons((uint16_t) packetsize);

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));
	oh1->class_num = 1;
	oh1->c_type = 1;

	//followed by a Session
	SessionClass* session = (SessionClass*) (oh1 + 1);
	session->dstAddress = DST;
	session->protocolId = this->m_Sender_Session_Protocol;
	session->flags = 0;
	session->dstPort = htons(DSTPORT);
	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->length = htons((uint16_t) (sizeof(RsvpHopClass) + sizeof(ObjectHeader)));
	oh2->class_num = 3;
	oh2->c_type = 1;
	//RSVPHop object
	RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
	hc->hopAdress = m_localIP;
	hc->logicalInterfaceHandle = 0;

	//Third object Header
	ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
	oh3->length = htons((uint16_t) (sizeof(TimeValues) + sizeof(ObjectHeader)));
	oh3->class_num = 5;
	oh3->c_type = 1;
	TimeValues * tv = (TimeValues*) (oh3 + 1);
	tv->R = htonl(30000); //Default = 30 seconds, take 5 seconds for this project, for testing purposes.
	this->m_local_state_lifetime = (double) (3.5 * 1.5 * ntohl(tv->R));
	//Fourth Object Header

	ObjectHeader * oh4 = (ObjectHeader*) (tv + 1);
	oh4->length = htons((uint16_t) (sizeof(RsvpSenderTemplate) + sizeof(ObjectHeader)));
	oh4->class_num = 11;
	oh4->c_type = 1;
	RsvpSenderTemplate* rstemplate = (RsvpSenderTemplate*) (oh4 + 1);
	rstemplate->srcAdress = SRC;
	rstemplate->srcPort = htons(SRCPORT);

	//Fifth Object header
	ObjectHeader * oh5 = (ObjectHeader*) (rstemplate + 1);
	oh5->c_type = 2;
	oh5->class_num = 12;
	oh5->length = htons((uint16_t) (sizeof(RsvpSenderTSpec) + sizeof(ObjectHeader)));
	RsvpSenderTSpec * rts = (RsvpSenderTSpec*) (oh5 + 1);
	rts->msgformvnumreserved = 0;
	rts->overallLength = htons(7);
	rts->serviceNumber = 1;
	rts->nreserved2 = 0;
	rts->lengthOfServiceData = htons(6);
	rts->parameterId = 127;
	rts->flags = 0;
	rts->fin = htons(5);
	rts->r = htonl(0x7f800000);
	rts->b = htonl(0x7f800000);
	rts->p = htonl(0x7f800000); //
	rts->m = htonl(1000); //Minimum size is 1000 bits (ECHO)
	rts->M = htonl(1000); //Max size is also 1000 because we only these ECHO

	//Setting the RSVP checksum.
	if (PsVector.size() == 0) {
		this->m_path_state.session = (*session);
		this->m_path_state.prev_hop_node = m_localIP;
		this->m_path_state.stemplate = (*rstemplate);
		this->m_path_state.ts = (*rts);
		this->m_path_set = true;
		this->m_path_state.L = (uint32_t) (3.5 * 1.5 * 30000);
		PsVector.push_back(this->m_path_state);
		this->m_path_set = true;
	}
	head->checksum = RsvpChecksum2(packet->data(), packetsize);
	return packet;
}

Packet* RsvpDaemon2::GenResv(IPAddress DST, uint16_t DSTPORT, uint8_t proto, uint16_t srcport, IPAddress srcaddr)
{
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 6 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass)
	        + sizeof(TimeValues) + sizeof(Style) + sizeof(RsvpFlowSpec) + sizeof(FilterSpec);
	int headroom = 0;
	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());
	//Fill in the common header
	CommonHeader* head = (CommonHeader*) (packet->data());

	head->verflags |= 1 << 4;
	head->msg_type = 2; // 2 = RESV
	head->checksum = 0; //0 for calculation purpose, fill in at end
	head->send_ttl = 30; //Should be the same as the one we just received, However, here we originate the path message.
	head->reserved = 0;
	head->Rsvp_length = htons(packetsize);

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));
	oh1->class_num = 1;
	oh1->c_type = 1;
	//int ObjectHeaderoffset = sizeof(ObjectHeader);
	//followed by a Session
	SessionClass* session = (SessionClass*) (oh1 + 1);
	session->dstAddress = DST;
	session->protocolId = proto;
	session->flags = 0;
	session->dstPort = htons(DSTPORT);

	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->length = htons((uint16_t) (sizeof(RsvpHopClass) + sizeof(ObjectHeader)));
	oh2->class_num = 3;
	oh2->c_type = 1;
	//RSVPHop object
	RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
	hc->hopAdress.s_addr = m_localIP;
	hc->logicalInterfaceHandle = 0;

	//Third object Header
	ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
	oh3->length = htons((uint16_t) (sizeof(TimeValues) + sizeof(ObjectHeader)));
	oh3->class_num = 5;
	oh3->c_type = 1;
	TimeValues * tv = (TimeValues*) (oh3 + 1);
	tv->R = htonl(30000);

	//Fourth Object Header
	ObjectHeader * oh4 = (ObjectHeader*) (tv + 1);
	oh4->length = htons((uint16_t) (sizeof(Style) + sizeof(ObjectHeader)));
	oh4->class_num = 8;	//TODO
	oh4->c_type = 1;	//TODO

	Style* stl = (Style*) (oh4 + 1);
	uint32_t flags = 0;
	flags |= 1 << 29;	//Set style FF
	flags |= 1 << 31;
	stl->flagsoptions = htonl(10);
	//Setting the RSVP checksum.

	ObjectHeader* oh5 = (ObjectHeader*) (stl + 1);
	oh5->length = htons((uint16_t) (sizeof(RsvpFlowSpec) + sizeof(ObjectHeader)));
	oh5->class_num = 9;
	oh5->c_type = 2;
	RsvpFlowSpec* rts = (RsvpFlowSpec*) (oh5 + 1);

	rts->msgformatvernumreserved = 0;
	rts->overallLength = htons(7);
	rts->serviceNumber = 5;
	rts->nreserved2 = 0;
	rts->lengthOfServiceData = htons(6);
	rts->parameterId = 127;
	rts->flags = 0;
	rts->fin = htons(5);
	rts->r = htonl(0x7f800000);
	rts->b = htonl(0x7f800000);
	rts->p = htonl(0x7f800000);
	rts->m = htonl(1000); //TODO:Should be the same as incoming TSPEC
	rts->M = htonl(1000); //All echos are 1000.

	ObjectHeader* oh6 = (ObjectHeader*) (rts + 1);
	oh6->length = htons((uint16_t) (sizeof(FilterSpec) + sizeof(ObjectHeader)));
	oh6->class_num = 10;
	oh6->c_type = 1;
	FilterSpec* rfs = (FilterSpec *) (oh6 + 1);
	rfs->srcAdress = srcaddr;
	rfs->srcPort = htons(srcport);

	head->checksum = RsvpChecksum2(packet->data(), packetsize);
	//head->checksum = click_in_cksum((unsigned char *)packet->data(), packetsize);
	return packet;
}

Packet* RsvpDaemon2::GenPathTear()
{
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 4 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass)
	        + sizeof(RsvpSenderTemplate) + sizeof(RsvpSenderTSpec);
	int headroom = 0;

	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());
	CommonHeader* head = (CommonHeader*) (packet->data());
	head->Rsvp_length = htons(packetsize);
	head->checksum = 0;
	head->verflags |= 1 << 4;
	head->msg_type = 5;	//5 = PathTear
	head->reserved = 0;
	head->send_ttl = 30;

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->c_type = 1;
	oh1->class_num = 1;
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));
	//followed by a Session object
	SessionClass* session = (SessionClass*) (oh1 + 1);
	if (m_RSVP_Sender) {
		session->dstAddress = m_Sender_Session_DST;
		session->dstPort = htons(m_Sender_Session_DSTPORT);
		session->flags = 0;
		session->protocolId = m_Sender_Session_Protocol;
	} else {
		session->dstAddress = this->m_path_state.session.dstAddress;
		session->dstPort = htons(this->m_path_state.session.dstPort);
		session->flags = 0;
		session->protocolId = this->m_path_state.session.protocolId;
	}

	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->c_type = 1;
	oh2->class_num = 3;
	oh2->length = htons((uint16_t) (sizeof(RsvpHopClass) + sizeof(ObjectHeader)));

	//RSVPHop object
	RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
	hc->hopAdress = m_localIP;
	hc->logicalInterfaceHandle = 0;

	//Third object Header
	ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
	oh3->c_type = 1;
	oh3->class_num = 11;
	oh3->length = htons((uint16_t) (sizeof(RsvpSenderTemplate) + sizeof(ObjectHeader)));
	//RSVPSender Template Object
	RsvpSenderTemplate* rstemplate = (RsvpSenderTemplate*) (oh3 + 1);
	if (m_RSVP_Sender) {
		rstemplate->srcAdress = m_localIP;
		rstemplate->srcPort = htons(m_packet_source_port);
		rstemplate->voidbits = 0;
	} else {
		*rstemplate = this->m_path_state.stemplate;	//Def copy constructor?
	}

	//Fourth Object header
	ObjectHeader * oh4 = (ObjectHeader*) (rstemplate + 1);
	oh4->c_type = 2;
	oh4->class_num = 12;
	oh4->length = htons((uint16_t) (sizeof(RsvpSenderTSpec) + sizeof(ObjectHeader)));

	RsvpSenderTSpec * rts = (RsvpSenderTSpec*) (oh4 + 1);
	rts->M = htonl(1000);
	rts->m = htonl(1000);
	rts->b = htonl(0x7f800000);
	rts->fin = htons(5);
	rts->flags = 0;
	rts->lengthOfServiceData = htons(6);
	rts->msgformvnumreserved = 0;
	rts->overallLength = htons(7);
	rts->p = htonl(0x7f800000);
	rts->parameterId = 127;
	rts->r = htonl(0x7f800000);
	rts->serviceNumber = 1;

	head->checksum = RsvpChecksum2(packet->data(), packetsize);
	if (m_RSVP_Sender) {
		Packet* packet2 = this->IpEncap(packet, 46, m_localIP, m_Sender_Session_DST, false);
		return packet2;
	} else {
		Packet* packet2 = this->IpEncap(packet, 46, m_localIP, this->m_path_state.session.dstAddress, false);
		return packet2;
	}

}

Packet* RsvpDaemon2::GenResvTear()
{
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 4 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass)
	        + sizeof(Style) + sizeof(FilterSpec);
	int headroom = 0;
	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());

	//Fill in the common header
	CommonHeader* head = (CommonHeader*) (packet->data());
	head->Rsvp_length = htons(packetsize);
	head->checksum = 0;
	head->verflags |= 1 << 4;
	head->msg_type = 6;
	head->reserved = 0;
	head->send_ttl = 250;

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->c_type = 1;
	oh1->class_num = 1;
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));

	//followed by a Session
	SessionClass* session = (SessionClass*) (oh1 + 1);
	session->dstAddress = this->m_path_state.session.dstAddress;
//	click_chatter("[%s] Setting the DST of the session to %s", inet_ntoa(m_localIP), inet_ntoa(this->m_path_state.session.dstAddress));
	session->dstPort = this->m_path_state.session.dstPort;
	session->flags = 0;
	session->protocolId = this->m_path_state.session.protocolId;

	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->c_type = 1;
	oh2->class_num = 3;
	oh2->length = htons((uint16_t) (sizeof(RsvpHopClass) + sizeof(ObjectHeader)));
	//RSVPHop object
	RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
	hc->hopAdress = m_localIP;
	hc->logicalInterfaceHandle = 0;

	//Third object Header
	ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
	oh3->c_type = 1;
	oh3->class_num = 8;
	oh3->length = htons((uint16_t) (sizeof(Style) + sizeof(ObjectHeader)));

	//Style Object
	Style* s = (Style*) (oh3 + 1);
	s->flagsoptions = htonl(10);

	ObjectHeader * oh5 = (ObjectHeader*) (s + 1);
	oh5->c_type = 1;
	oh5->class_num = 10;
	oh5->length = htons((uint16_t) (sizeof(FilterSpec) + sizeof(ObjectHeader)));
	FilterSpec* fis = (FilterSpec*) (oh5 + 1);
	fis->srcAdress.s_addr = 0;	//Filterspec is ignored in RESVTEAR
	fis->srcPort = htons(0);	//Filterspec is ignored in RESVTEAR

	head->checksum = click_in_cksum(packet->data(), packetsize);

	Packet * packet2 = this->IpEncap(packet, 46, m_localIP, this->m_path_state.prev_hop_node, false);
	//click_chatter("[%s] Pushing out a ResvTear.", inet_ntoa(m_localIP));
	return packet2;
}

Packet* RsvpDaemon2::GenPathErr(uint8_t ErrorCode, uint16_t ErrorValue)
{
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 2 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(ErrorSpec);

	int headroom = 0;	//No need for header space, we add it in the click script.
	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());
	//Fill in the common header
	CommonHeader* head = (CommonHeader*) (packet->data());

	head->verflags |= 1 << 4;
	head->msg_type = 3;	// 3 = PathErr
	head->checksum = 0;	//0 for calculation purpose, fill in at end
	head->send_ttl = 30; //Should be the same as the one we just received, However, here we originate the path message.
	head->reserved = 0;
	head->Rsvp_length = htons(packetsize);

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));
	oh1->class_num = 1;
	oh1->c_type = 1;
	//followed by a Session
	SessionClass* session = (SessionClass*) (oh1 + 1);
	session->dstAddress = this->m_path_state.session.dstAddress;

	session->protocolId = this->m_path_state.session.protocolId;
	session->flags = 0;
	session->dstPort = this->m_path_state.session.dstPort;

	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->length = htons((uint16_t) (sizeof(ErrorSpec) + sizeof(ObjectHeader)));
	oh2->class_num = 6;
	oh2->c_type = 1;
	//ErrorSpec object
	ErrorSpec* es = (ErrorSpec*) (oh2 + 1);
	es->errorNodeAddress = m_localIP;
	es->flags = 0;
	es->errorCode = ErrorCode;
	es->errorValue = htons(ErrorValue);
	//Setting the RSVP checksum.

	head->checksum = 0;
	head->checksum = click_in_cksum((unsigned char*) packet->data(), packetsize);
	Packet * p2 = this->IpEncap(packet, 46, m_localIP, this->m_path_state.prev_hop_node, false);
	return p2;
}

Packet* RsvpDaemon2::GenResvErr(uint8_t ErrorCode, uint16_t ErrorValue)
{
	/*
	 * EC:
	 * 0 = ResvConf
	 * 6 = Unknown Reservation Style
	 * 9/10/11 = Reserved
	 * 13 = Unknown Object Class
	 * 14 = Unkonwn Object Ctype
	 * 15-19 = Reserved
	 * 20 = API Error
	 * 21 = Traffic Control Error
	 *
	 * For 21: Error Value field, lowest bits:
	 * 01 = Service conflict
	 * 02 = Service Unsupported
	 * 03 = Bad Flow spec
	 * 04 = Bad Tspec
	 *
	 * 23 = RSVP System Error
	 *
	 */
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 4 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass)
	        + sizeof(ErrorSpec) + sizeof(Style);
	//int headroom = sizeof(click_ip)+ sizeof(click_udp);
	int headroom = 0;	//No need for header space, we add it in the click script.
	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());
	//Fill in the common header
	CommonHeader* head = (CommonHeader*) (packet->data());

	head->verflags |= 1 << 4;
	head->msg_type = 4;	// 4 = ResvErr
	head->checksum = 0;	//0 for calculation purpose, fill in at end
	head->send_ttl = 30; //Should be the same as the one we just received, However, here we originate the path message.
	head->reserved = 0;
	head->Rsvp_length = htons(packetsize);

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));
	oh1->class_num = 1;
	oh1->c_type = 1;
	//followed by a Session
	SessionClass* session = (SessionClass*) (oh1 + 1);
	session->dstAddress = this->m_path_state.session.dstAddress;
	session->protocolId = this->m_path_state.session.protocolId;
	session->flags = 0;
	session->dstPort = this->m_path_state.session.dstPort;

	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->length = htons((uint16_t) (sizeof(RsvpHopClass) + sizeof(ObjectHeader)));
	oh2->class_num = 3;
	oh2->c_type = 1;
	//RSVPHop object
	RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
	hc->hopAdress = m_localIP;
	hc->logicalInterfaceHandle = 0;

	//Third object header
	ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
	oh3->length = htons((uint16_t) (sizeof(ErrorSpec) + sizeof(ObjectHeader)));
	oh3->class_num = 6;
	oh3->c_type = 1;
	//ErrorSpec object
	ErrorSpec* es = (ErrorSpec*) (oh3 + 1);
	es->errorNodeAddress = m_localIP;
	es->flags = 0;
	es->errorCode = ErrorCode;
	es->errorValue = ErrorValue;

	//Fourth Object Header
	ObjectHeader * oh4 = (ObjectHeader*) (es + 1);
	oh4->length = htons((uint16_t) (sizeof(Style) + sizeof(ObjectHeader)));
	oh4->class_num = 8;
	oh4->c_type = 1;
	Style* stl = (Style*) (oh4 + 1);
	stl->flagsoptions = htonl(10);

	//Setting the RSVP checksum.
	head->checksum = 0;
	head->checksum = RsvpChecksum2(packet->data(), packetsize);
	Packet * p2 = this->IpEncap(packet, 46, m_localIP, this->m_next_hop, false);
	return p2;
}

Packet * RsvpDaemon2::GenResvConf(IPAddress ip_of_receiver_that_requested_conf)
{
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 5 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(ErrorSpec)
	        + sizeof(ResvConfirm) + sizeof(Style) + sizeof(FilterSpec);

	int headroom = 0;
	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());

	//Fill in the common header
	CommonHeader* head = (CommonHeader*) (packet->data());

	head->verflags |= 1 << 4;
	head->msg_type = 7;	// 7 = Confirmation
	head->checksum = 0;	//0 for calculation purpose, fill in at end
	head->send_ttl = 30; //Should be the same as the one we just received, However, here we originate the path message.
	head->reserved = 0;
	head->Rsvp_length = htons(packetsize);

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));
	oh1->class_num = 1;
	oh1->c_type = 1;

	//followed by a Session
	SessionClass* session = (SessionClass*) (oh1 + 1);
	session->dstAddress = this->m_Sender_Session_DST;
	session->protocolId = this->m_Sender_Session_Protocol;	//
	session->flags = 0;
	session->dstPort = htons(this->m_Sender_Session_DSTPORT);

	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->length = htons((uint16_t) (sizeof(ErrorSpec) + sizeof(ObjectHeader)));
	oh2->class_num = 6;
	oh2->c_type = 1;
	//ErrorSpec object
	ErrorSpec* es = (ErrorSpec*) (oh2 + 1);
	es->errorCode = 0;
	es->errorNodeAddress = m_localIP;
	es->errorValue = 0;
	es->flags = 0;

	//Third object header
	ObjectHeader * oh3 = (ObjectHeader*) (es + 1);
	oh3->length = htons((uint16_t) (sizeof(ResvConfirm) + sizeof(ObjectHeader)));
	oh3->class_num = 15;
	oh3->c_type = 1;
	//ErrorSpec object
	ResvConfirm* rc = (ResvConfirm*) (oh3 + 1);
	rc->receiverAddress = ip_of_receiver_that_requested_conf;

	//Fourth Object Header
	ObjectHeader * oh4 = (ObjectHeader*) (rc + 1);
	oh4->length = htons((uint16_t) (sizeof(Style) + sizeof(ObjectHeader)));
	oh4->class_num = 8;
	oh4->c_type = 1;
	Style* stl = (Style*) (oh4 + 1);
	stl->flagsoptions = htonl(10);

	ObjectHeader * oh5 = (ObjectHeader*) (stl + 1);
	oh5->length = htons((uint16_t) (sizeof(FilterSpec) + sizeof(ObjectHeader)));
	oh5->class_num = 10;
	oh5->c_type = 1;
	FilterSpec* fs = (FilterSpec*) (oh5 + 1);
	fs->srcAdress = m_localIP;
	fs->srcPort = this->m_packet_source_port;
	fs->voidbits = 0;

	//Setting the RSVP checksum.
	head->checksum = RsvpChecksum2(packet->data(), packetsize);
	Packet * packet2 = this->IpEncap(packet, 46, m_localIP, this->m_next_hop, false);
	return packet2;
}

void RsvpDaemon2::InitRsvp(IPAddress packet_src, IPAddress packet_dst, uint16_t s_port, uint16_t d_port)
{
	// The only time we enter this, is when the sender wants to start RSVP with
	// The packet being one of the UDP sources, so we can copy the IP'S
	this->m_rsvp_started = true;
	Packet * pathinit = this->GenPath(packet_src, s_port, packet_dst, d_port);
	Packet * ippath = this->IpEncap(pathinit, 46, packet_src, packet_dst, false);
	output(0).push(ippath);

}

void RsvpDaemon2::SenderInitTearDown()
{

	Packet * pathtear = this->GenPathTear();
	this->m_path_set = false;
	//Path tear needs to shutdown the current session.
	click_chatter("[%s] Pushing out a Path Tear.", inet_ntoa(m_localIP));
	output(0).push(pathtear);

}

void RsvpDaemon2::ReceiverInitTearDown()
{
	Packet * resvTear = this->GenResvTear();
	click_chatter("[%s] Initiating ResvTear. Pushing out a ResvTear.", inet_ntoa(m_localIP));
	output(0).push(resvTear);
}

Packet * RsvpDaemon2::IpEncap(Packet* p, int proto, IPAddress src, IPAddress dst, bool RAP)
{
	/*
	 * This implementation of IPEncap was taken from an original implementation supplied by Gitte.
	 * After multiple questions to Johan Bergs, I was unable to integrate the click element and make use of
	 * its methods from within my own Element, and I could not possibly copy all the code used by IPEncap itself.
	 * not want to duplicate all the code.
	 *
	 * Code has been adapted to fit the needs of the project, but can be produced if required.
	 */
	int packetsize = p->length();
	int headroom = sizeof(click_ip) + sizeof(click_ether);
	if (RAP) {
		headroom += sizeof(RouterAlertIP);
	}
	if (WritablePacket* packet = Packet::make(headroom, 0, packetsize, 0)) {
		memcpy(packet->data(), p->data(), packetsize);
		if (packet = packet->push(sizeof(click_ip))) {	//Returns 0 if fails
			memset(packet->data(), 0, sizeof(click_ip));	//Alles v/d ipheader op 0
			click_ip* iph = (click_ip*) packet->data();
			iph->ip_v = 4;
			iph->ip_hl = 5;
			iph->ip_tos = 0;
			iph->ip_len = htons(sizeof(click_ip) + packetsize);
			iph->ip_ttl = 250;
			iph->ip_p = proto;
			iph->ip_src = src;
			iph->ip_dst = dst;
			iph->ip_id = htons(m_ip_id_count);
			iph->ip_sum = 0; //For CS calculation Purposes.
			m_ip_id_count++;
			if (RAP) {
				RouterAlertIP * raip = (RouterAlertIP *) (iph + 1);
				raip->value = 0;
			}
			iph->ip_sum = click_in_cksum((unsigned char*) packet->data(), packet->length());

			packet->set_network_header((unsigned char *) iph, sizeof(click_ip));
			//Tell Click to set the Network Header pointer,
			//otherwise IPGW fails the packet->has_transport_header() assert.
			p->kill();
			packet->set_dst_ip_anno(iph->ip_dst);
			return packet;
		}
	}
	return 0;
}

void RsvpDaemon2::push(int int1, Packet* p)
{
	//Generate one of each packet.
	//Push it out.
	Packet * packet = this->GenPath(IPAddress("2.2.2.2"), 1111, IPAddress("3.3.3.3"), 3333);
	Packet * encapped = this->IpEncap(packet, 46, IPAddress("2.2.2.2"), IPAddress("3.3.3.3"), false);
	output(0).push(encapped);
	packet = this->GenResv(IPAddress("2.2.2.2"), 1111,17, IPAddress("3.3.3.3"), 3333);
	encapped = this->IpEncap(packet,46, IPAddress("2.2.2.2"), IPAddress("3.3.3.3"),false);
	output(0).push(encapped);
	packet = this->GenPathErr(1,1);
	output(0).push(packet);
	packet = this->GenResvErr(1,1);
	output(0).push(packet);
	packet = this->GenPathTear();
	output(0).push(packet);
	packet = this->GenResvTear();
	output(0).push(packet);
	packet = this->GenResvConf(IPAddress("2.2.2.2"));
	output(0).push(packet);



}
/*
 * http://stackoverflow.com/questions/199333/how-to-detect-integer-overflow-in-c-c
 */
size_t highestOneBitPosition2(uint16_t a)
{
	size_t bits = 0;
	while (a != 0) {
		++bits;
		a >>= 1;
	};
	return bits;
}

bool addition_is_safe2(uint16_t a, uint16_t b)
{
	size_t a_bits = highestOneBitPosition2(a), b_bits = highestOneBitPosition2(b);
	return (a_bits < 16 && b_bits < 16);
}

uint16_t RsvpChecksum2(unsigned char * message, int msg_size)
{
	/*
	 * http://www.faqs.org/rfcs/rfc1071.html
	 * This RFC shows optimal way of calculating the Checksum for the RSVP Message
	 */
//msg_size in bytes.
//All packets and header are formed using 32 bits each. Therefore, we must be returned
//a value which is divisible by 4 (8*4 bits = 32)
//Checksum should be set to 0 for calculation.
//message is a pointer to the packet. We need to access all bytes.
//Checksum is the 1's complement of:
//the 1's complement sum of the message, and the checksum, set to 0 for the calculation;
	assert(msg_size % 4 == 0);
	uint32_t checksm = 0;
	uint16_t *head = reinterpret_cast<uint16_t*>(message);
	for (int i = 1; i <= msg_size / 2; i++) {
		if (addition_is_safe2(checksm, (*head))) {
			//click_chatter("addition is safe");
			checksm += (*head);
		} else {
			//Find a way to make sure "overflow" has occurred.
			//click_chatter("Addition is unsafe");
			checksm += (*head);
			//checksm++;
		}

		head = head + 1; //Move up 2 bytes.
	}
	while (checksm != (uint16_t) checksm) {
		uint32_t tmp = checksm;
		uint16_t lowest = checksm & 0x0000ffff;
		uint16_t highest = checksm >> 16;
		checksm = lowest + highest;
		//click_chatter("RFC 1071 Method: %zu ||| Final Checksum:%zu", tmp, checksm);
	}
//click_chatter("Returning Checksm");
	return ~checksm; //1's complement.
}

void RsvpDaemon2::run_timer(Timer* t)
{
	this->m_PathInit_timer.reschedule_after_msec(100000);
}
CLICK_ENDDECLS
EXPORT_ELEMENT(RsvpDaemon2)
