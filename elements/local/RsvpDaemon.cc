#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/udp.h>
#include <clicknet/ether.h>
#include <clicknet/ip.h>
#include <elements/ip/ipencap.hh>
#include <arpa/inet.h>

#include "RsvpDaemon.hh"
#include "RsvpSenderTemplate.hh"
#include "RsvpSenderTSpec.hh"
#include "CommonHeader.hh"
#include "ObjectHeader.hh"
#include "ObjectHeader.hh"
#include "SessionClass.hh"
#include "RsvpHopClass.hh"
#include "TimeValues.hh"
#include "Style.hh"
#include "FilterSpec.hh"
#include "RsvpFlowSpec.hh"
#include "ErrorSpec.hh"
#include "ResvConfirm.hh"
#include "RouterAlertIP.hh"

#define PATH_SIZE sizeof(CommonHeader) + 5*sizeof(ObjectHeader) + sizeof(SessionClass)+ sizeof(RsvpHopClass) + sizeof(TimeValues) + sizeof(RsvpSenderTemplate) + sizeof(RsvpSenderTSpec)
#define RESV_SIZE sizeof(CommonHeader) + 6*sizeof(ObjectHeader) + sizeof(SessionClass)+ sizeof(RsvpHopClass) + sizeof(TimeValues) + sizeof(Style) + sizeof(RsvpFlowSpec) + sizeof(FilterSpec)
#define RESV_TEAR_SIZE sizeof(CommonHeader) + 4 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass) + sizeof(Style) + sizeof(FilterSpec)
#define PATH_ERR_SIZE sizeof(CommonHeader) + 2 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(ErrorSpec)
#define RESV_ERR_SIZE sizeof(CommonHeader) + 4 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass) + sizeof(ErrorSpec) + sizeof(Style)
#define RESV_CONF_SIZE sizeof(CommonHeader) + 5 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(ErrorSpec)+ sizeof(ResvConfirm) + sizeof(Style) + sizeof(FilterSpec)
CLICK_DECLS
uint16_t RsvpChecksum(unsigned char * message, int msg_size);

RsvpDaemon::RsvpDaemon()
	: m_PathInit_timer(this)
{
	// TODO Auto-generated constructor stub

}

RsvpDaemon::~RsvpDaemon()
{
	// TODO Auto-generated destructor stub
}

int RsvpDaemon::initialize(ErrorHandler* errh)
{
	return 0;
}

bool RsvpDaemon::CleanupPathState(SessionClass s, RsvpSenderTemplate t)
{
	Vector<PathState>::iterator del = 0;
	for (Vector<PathState>::iterator it = PsVector.begin(); it != PsVector.end(); it++) {
		if ((*it).session.dstAddress == s.dstAddress && (*it).session.dstPort == s.dstPort
		        && (*it).session.protocolId == s.protocolId && (*it).stemplate.srcAdress == t.srcAdress
		        && (*it).stemplate.srcPort == t.srcPort) {
			del = it;
			if (m_verbose) {
				click_chatter("[%s] Found a state to delete.", inet_ntoa(m_localIP));
			}
		}
	}
	if (del != 0) {
		PsVector.erase(del);
	} else {
		return false;
	}
	memset(&this->m_path_state, 0, sizeof(PathState));
	this->m_path_set = false;
	return true;
}
int RsvpDaemon::configure(Vector<String>& conf, ErrorHandler* errh)
{
	//RSVP should know its own address, whether it should start up RSVP or not,
	if (cp_va_kparse(conf, this, errh, "ADDRESS", cpkM, cpIPAddress, &m_localIP, "PRIOPORT", cpkN, cpInteger,
	        &m_prio_port, "VERBOSE", cpkN, cpBool, &m_verbose, "DESTINATION", cpkN, cpIPAddress,
	        &m_Sender_Session_DST, "PROTOCOL", cpkN, cpInteger, &m_Sender_Session_Protocol, cpEnd) < 0) {
		return -1;
	}

	m_RSVP_Sender = false;
	m_RSVP_Receiver = false;
	m_first_time = true;
	m_give_prio = false;
	m_rsvp_started = false;
	m_Sender_init_breakoff = false;
	m_Receiver_init_breakoff = false;
	m_path_set = false;
	m_resv_set = false;
	m_periodic_path_sending = false;

	m_path_state.path_state_set = false;
	m_path_state.prev_hop_node = 0;

	m_ip_id_count = 1; //For IP Id field.
	m_sender_period_count = 0;

	m_PathInit_timer.initialize(this);
	m_PathInit_timer.schedule_after_msec(5000); //Timer for initial PATH messages by active sender
	return 0;
}

Packet * RsvpDaemon::GenPath(IPAddress SRC, uint16_t SRCPORT, IPAddress DST, uint16_t DSTPORT)
{
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 5 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass)
	        + sizeof(TimeValues) + sizeof(RsvpSenderTemplate) + sizeof(RsvpSenderTSpec);
	int headroom = 0;
	//int headroom = 0;//No need for header space, we add it in the click script.
	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());
	//Fill in the common header
	CommonHeader* head = (CommonHeader*) (packet->data());

	head->verflags |= 1 << 4; //Version is 1, Flags are 0;
	head->msg_type = 1; // 1 = Path
	head->checksum = 0; //0 for calculation purpose, fill in at end
	head->send_ttl = 250; //Should be the same as the IP TTL field.
	head->reserved = 0;
	head->Rsvp_length = htons((uint16_t) packetsize);

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));
	oh1->class_num = 1;
	oh1->c_type = 1;

	//followed by a Session
	SessionClass* session = (SessionClass*) (oh1 + 1);
	session->dstAddress = DST;
	session->protocolId = this->m_Sender_Session_Protocol;
	session->flags = 0;
	session->dstPort = htons(DSTPORT);
	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->length = htons((uint16_t) (sizeof(RsvpHopClass) + sizeof(ObjectHeader)));
	oh2->class_num = 3;
	oh2->c_type = 1;
	//RSVPHop object
	RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
	hc->hopAdress = m_localIP;
	hc->logicalInterfaceHandle = 0;

	//Third object Header
	ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
	oh3->length = htons((uint16_t) (sizeof(TimeValues) + sizeof(ObjectHeader)));
	oh3->class_num = 5;
	oh3->c_type = 1;
	TimeValues * tv = (TimeValues*) (oh3 + 1);
	tv->R = htonl(30000); //Default = 30 seconds, take 5 seconds for this project, for testing purposes.
	this->m_local_state_lifetime = (double) (3.5 * 1.5 * ntohl(tv->R));
	//Fourth Object Header

	ObjectHeader * oh4 = (ObjectHeader*) (tv + 1);
	oh4->length = htons((uint16_t) (sizeof(RsvpSenderTemplate) + sizeof(ObjectHeader)));
	oh4->class_num = 11;
	oh4->c_type = 1;
	RsvpSenderTemplate* rstemplate = (RsvpSenderTemplate*) (oh4 + 1);
	rstemplate->srcAdress = SRC;
	rstemplate->srcPort = htons(SRCPORT);

	//Fifth Object header
	ObjectHeader * oh5 = (ObjectHeader*) (rstemplate + 1);
	oh5->c_type = 2;
	oh5->class_num = 12;
	oh5->length = htons((uint16_t) (sizeof(RsvpSenderTSpec) + sizeof(ObjectHeader)));
	RsvpSenderTSpec * rts = (RsvpSenderTSpec*) (oh5 + 1);
	rts->msgformvnumreserved = 0;
	rts->overallLength = htons(7);
	rts->serviceNumber = 1;
	rts->nreserved2 = 0;
	rts->lengthOfServiceData = htons(6);
	rts->parameterId = 127;
	rts->flags = 0;
	rts->fin = htons(5);
	rts->r = htonl(0x7f800000);
	rts->b = htonl(0x7f800000);
	rts->p = htonl(0x7f800000); //
	rts->m = htonl(1000); //Minimum size is 1000 bits (ECHO)
	rts->M = htonl(1000); //Max size is also 1000 because we only these ECHO

	//Setting the RSVP checksum.
	if (PsVector.size() == 0) {
		this->m_path_state.session = (*session);
		this->m_path_state.prev_hop_node = m_localIP;
		this->m_path_state.stemplate = (*rstemplate);
		this->m_path_state.ts = (*rts);
		this->m_path_set = true;
		this->m_path_state.path_state_set = true;
		this->m_path_state.L = (uint32_t) (3.5 * 1.5 * 30000);
		PsVector.push_back(this->m_path_state);
	}
	head->checksum = RsvpChecksum(packet->data(), packetsize);
	return packet;
}

Packet* RsvpDaemon::GenResv(IPAddress DST, uint16_t DSTPORT, uint8_t proto, uint16_t srcport, IPAddress srcaddr)
{
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 6 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass)
	        + sizeof(TimeValues) + sizeof(Style) + sizeof(RsvpFlowSpec) + sizeof(FilterSpec);
	int headroom = 0;
	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());
	//Fill in the common header
	CommonHeader* head = (CommonHeader*) (packet->data());

	head->verflags |= 1 << 4;
	head->msg_type = 2; // 2 = RESV
	head->checksum = 0; //0 for calculation purpose, fill in at end
	head->send_ttl = 30; //Should be the same as the one we just received, However, here we originate the path message.
	head->reserved = 0;
	head->Rsvp_length = htons(packetsize);

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));
	oh1->class_num = 1;
	oh1->c_type = 1;
	//int ObjectHeaderoffset = sizeof(ObjectHeader);
	//followed by a Session
	SessionClass* session = (SessionClass*) (oh1 + 1);
	session->dstAddress = DST;
	session->protocolId = proto;
	session->flags = 0;
	session->dstPort = htons(DSTPORT);

	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->length = htons((uint16_t) (sizeof(RsvpHopClass) + sizeof(ObjectHeader)));
	oh2->class_num = 3;
	oh2->c_type = 1;
	//RSVPHop object
	RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
	hc->hopAdress.s_addr = m_localIP;
	hc->logicalInterfaceHandle = 0;

	//Third object Header
	ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
	oh3->length = htons((uint16_t) (sizeof(TimeValues) + sizeof(ObjectHeader)));
	oh3->class_num = 5;
	oh3->c_type = 1;
	TimeValues * tv = (TimeValues*) (oh3 + 1);
	tv->R = htonl(30000);

	//Fourth Object Header
	ObjectHeader * oh4 = (ObjectHeader*) (tv + 1);
	oh4->length = htons((uint16_t) (sizeof(Style) + sizeof(ObjectHeader)));
	oh4->class_num = 8;	//TODO
	oh4->c_type = 1;	//TODO

	Style* stl = (Style*) (oh4 + 1);
	uint32_t flags = 0;
	flags |= 1 << 29;	//Set style FF
	flags |= 1 << 31;
	stl->flagsoptions = htonl(10);
	//Setting the RSVP checksum.

	ObjectHeader* oh5 = (ObjectHeader*) (stl + 1);
	oh5->length = htons((uint16_t) (sizeof(RsvpFlowSpec) + sizeof(ObjectHeader)));
	oh5->class_num = 9;
	oh5->c_type = 2;
	RsvpFlowSpec* rts = (RsvpFlowSpec*) (oh5 + 1);

	rts->msgformatvernumreserved = 0;
	rts->overallLength = htons(7);
	rts->serviceNumber = 5;
	rts->nreserved2 = 0;
	rts->lengthOfServiceData = htons(6);
	rts->parameterId = 127;
	rts->flags = 0;
	rts->fin = htons(5);
	rts->r = htonl(0x7f800000);
	rts->b = htonl(0x7f800000);
	rts->p = htonl(0x7f800000);
	rts->m = htonl(1000); //TODO:Should be the same as incoming TSPEC
	rts->M = htonl(1000); //All echos are 1000.

	ObjectHeader* oh6 = (ObjectHeader*) (rts + 1);
	oh6->length = htons((uint16_t) (sizeof(FilterSpec) + sizeof(ObjectHeader)));
	oh6->class_num = 10;
	oh6->c_type = 1;
	FilterSpec* rfs = (FilterSpec *) (oh6 + 1);
	rfs->srcAdress = srcaddr;
	rfs->srcPort = htons(srcport);

	head->checksum = RsvpChecksum(packet->data(), packetsize);
	//head->checksum = click_in_cksum((unsigned char *)packet->data(), packetsize);
	return packet;
}

Packet* RsvpDaemon::GenPathTear()
{
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 4 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass)
	        + sizeof(RsvpSenderTemplate) + sizeof(RsvpSenderTSpec);
	int headroom = 0;

	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());
	CommonHeader* head = (CommonHeader*) (packet->data());
	head->Rsvp_length = htons(packetsize);
	head->checksum = 0;
	head->verflags |= 1 << 4;
	head->msg_type = 5;	//5 = PathTear
	head->reserved = 0;
	head->send_ttl = 30;

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->c_type = 1;
	oh1->class_num = 1;
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));
	//followed by a Session object
	SessionClass* session = (SessionClass*) (oh1 + 1);
	if (m_RSVP_Sender) {
		session->dstAddress = m_Sender_Session_DST;
		session->dstPort = htons(m_Sender_Session_DSTPORT);
		session->flags = 0;
		session->protocolId = m_Sender_Session_Protocol;
	} else {
		session->dstAddress = this->m_path_state.session.dstAddress;
		session->dstPort = htons(this->m_path_state.session.dstPort);
		session->flags = 0;
		session->protocolId = this->m_path_state.session.protocolId;
	}

	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->c_type = 1;
	oh2->class_num = 3;
	oh2->length = htons((uint16_t) (sizeof(RsvpHopClass) + sizeof(ObjectHeader)));

	//RSVPHop object
	RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
	hc->hopAdress = m_localIP;
	hc->logicalInterfaceHandle = 0;

	//Third object Header
	ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
	oh3->c_type = 1;
	oh3->class_num = 11;
	oh3->length = htons((uint16_t) (sizeof(RsvpSenderTemplate) + sizeof(ObjectHeader)));
	//RSVPSender Template Object
	RsvpSenderTemplate* rstemplate = (RsvpSenderTemplate*) (oh3 + 1);
	if (m_RSVP_Sender) {
		rstemplate->srcAdress = m_localIP;
		rstemplate->srcPort = htons(m_packet_source_port);
		rstemplate->voidbits = 0;
	} else {
		*rstemplate = this->m_path_state.stemplate;	//Def copy constructor?
	}

	//Fourth Object header
	ObjectHeader * oh4 = (ObjectHeader*) (rstemplate + 1);
	oh4->c_type = 2;
	oh4->class_num = 12;
	oh4->length = htons((uint16_t) (sizeof(RsvpSenderTSpec) + sizeof(ObjectHeader)));

	RsvpSenderTSpec * rts = (RsvpSenderTSpec*) (oh4 + 1);
	rts->M = htonl(1000);
	rts->m = htonl(1000);
	rts->b = htonl(0x7f800000);
	rts->fin = htons(5);
	rts->flags = 0;
	rts->lengthOfServiceData = htons(6);
	rts->msgformvnumreserved = 0;
	rts->overallLength = htons(7);
	rts->p = htonl(0x7f800000);
	rts->parameterId = 127;
	rts->r = htonl(0x7f800000);
	rts->serviceNumber = 1;

	head->checksum = RsvpChecksum(packet->data(), packetsize);
	if (m_RSVP_Sender) {
		Packet* packet2 = this->IpEncap(packet, 46, m_localIP, m_Sender_Session_DST, false);
		return packet2;
	} else {
		Packet* packet2 = this->IpEncap(packet, 46, m_localIP, this->m_path_state.session.dstAddress, false);
		return packet2;
	}

}

Packet* RsvpDaemon::GenResvTear()
{
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 4 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass)
	        + sizeof(Style) + sizeof(FilterSpec);
	int headroom = 0;
	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());

	//Fill in the common header
	CommonHeader* head = (CommonHeader*) (packet->data());
	head->Rsvp_length = htons(packetsize);
	head->checksum = 0;
	head->verflags |= 1 << 4;
	head->msg_type = 6;
	head->reserved = 0;
	head->send_ttl = 250;

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->c_type = 1;
	oh1->class_num = 1;
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));

	//followed by a Session
	SessionClass* session = (SessionClass*) (oh1 + 1);
	session->dstAddress = this->m_path_state.session.dstAddress;
	session->dstPort = this->m_path_state.session.dstPort;
	session->flags = 0;
	session->protocolId = this->m_path_state.session.protocolId;

	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->c_type = 1;
	oh2->class_num = 3;
	oh2->length = htons((uint16_t) (sizeof(RsvpHopClass) + sizeof(ObjectHeader)));
	//RSVPHop object
	RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
	hc->hopAdress = m_localIP;
	hc->logicalInterfaceHandle = 0;

	//Third object Header
	ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
	oh3->c_type = 1;
	oh3->class_num = 8;
	oh3->length = htons((uint16_t) (sizeof(Style) + sizeof(ObjectHeader)));

	//Style Object
	Style* s = (Style*) (oh3 + 1);
	s->flagsoptions = htonl(10);

	ObjectHeader * oh5 = (ObjectHeader*) (s + 1);
	oh5->c_type = 1;
	oh5->class_num = 10;
	oh5->length = htons((uint16_t) (sizeof(FilterSpec) + sizeof(ObjectHeader)));
	FilterSpec* fis = (FilterSpec*) (oh5 + 1);
	fis->srcAdress.s_addr = 0;	//Filterspec is ignored in RESVTEAR
	fis->srcPort = htons(0);	//Filterspec is ignored in RESVTEAR

	head->checksum = click_in_cksum(packet->data(), packetsize);

	Packet * packet2 = this->IpEncap(packet, 46, m_localIP, this->m_path_state.prev_hop_node, false);
	return packet2;
}

Packet* RsvpDaemon::GenPathErr(uint8_t ErrorCode, uint16_t ErrorValue)
{
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 2 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(ErrorSpec);

	int headroom = 0;	//No need for header space, we add it in the click script.
	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());
	//Fill in the common header
	CommonHeader* head = (CommonHeader*) (packet->data());

	head->verflags |= 1 << 4;
	head->msg_type = 3;	// 3 = PathErr
	head->checksum = 0;	//0 for calculation purpose, fill in at end
	head->send_ttl = 30; //Should be the same as the one we just received, However, here we originate the path message.
	head->reserved = 0;
	head->Rsvp_length = htons(packetsize);

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));
	oh1->class_num = 1;
	oh1->c_type = 1;
	//followed by a Session
	SessionClass* session = (SessionClass*) (oh1 + 1);
	session->dstAddress = this->m_path_state.session.dstAddress;

	session->protocolId = this->m_path_state.session.protocolId;
	session->flags = 0;
	session->dstPort = this->m_path_state.session.dstPort;

	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->length = htons((uint16_t) (sizeof(ErrorSpec) + sizeof(ObjectHeader)));
	oh2->class_num = 6;
	oh2->c_type = 1;
	//ErrorSpec object
	ErrorSpec* es = (ErrorSpec*) (oh2 + 1);
	es->errorNodeAddress = m_localIP;
	es->flags = 0;
	es->errorCode = ErrorCode;
	es->errorValue = htons(ErrorValue);
	//Setting the RSVP checksum.

	head->checksum = 0;
	head->checksum = click_in_cksum((unsigned char*) packet->data(), packetsize);
	Packet * p2 = this->IpEncap(packet, 46, m_localIP, this->m_path_state.prev_hop_node, false);
	return p2;
}

Packet* RsvpDaemon::GenResvErr(uint8_t ErrorCode, uint16_t ErrorValue)
{
	/*
	 * EC:
	 * 0 = ResvConf
	 * 6 = Unknown Reservation Style
	 * 9/10/11 = Reserved
	 * 13 = Unknown Object Class
	 * 14 = Unkonwn Object Ctype
	 * 15-19 = Reserved
	 * 20 = API Error
	 * 21 = Traffic Control Error
	 *
	 * For 21: Error Value field, lowest bits:
	 * 01 = Service conflict
	 * 02 = Service Unsupported
	 * 03 = Bad Flow spec
	 * 04 = Bad Tspec
	 *
	 * 23 = RSVP System Error
	 *
	 */
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 4 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(RsvpHopClass)
	        + sizeof(ErrorSpec) + sizeof(Style);
	//int headroom = sizeof(click_ip)+ sizeof(click_udp);
	int headroom = 0;	//No need for header space, we add it in the click script.
	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());
	//Fill in the common header
	CommonHeader* head = (CommonHeader*) (packet->data());

	head->verflags |= 1 << 4;
	head->msg_type = 4;	// 4 = ResvErr
	head->checksum = 0;	//0 for calculation purpose, fill in at end
	head->send_ttl = 30; //Should be the same as the one we just received, However, here we originate the path message.
	head->reserved = 0;
	head->Rsvp_length = htons(packetsize);

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));
	oh1->class_num = 1;
	oh1->c_type = 1;
	//followed by a Session
	SessionClass* session = (SessionClass*) (oh1 + 1);
	session->dstAddress = this->m_path_state.session.dstAddress;
	session->protocolId = this->m_path_state.session.protocolId;
	session->flags = 0;
	session->dstPort = this->m_path_state.session.dstPort;

	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->length = htons((uint16_t) (sizeof(RsvpHopClass) + sizeof(ObjectHeader)));
	oh2->class_num = 3;
	oh2->c_type = 1;
	//RSVPHop object
	RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
	hc->hopAdress = m_localIP;
	hc->logicalInterfaceHandle = 0;

	//Third object header
	ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
	oh3->length = htons((uint16_t) (sizeof(ErrorSpec) + sizeof(ObjectHeader)));
	oh3->class_num = 6;
	oh3->c_type = 1;
	//ErrorSpec object
	ErrorSpec* es = (ErrorSpec*) (oh3 + 1);
	es->errorNodeAddress = m_localIP;
	es->flags = 0;
	es->errorCode = ErrorCode;
	es->errorValue = ErrorValue;

	//Fourth Object Header
	ObjectHeader * oh4 = (ObjectHeader*) (es + 1);
	oh4->length = htons((uint16_t) (sizeof(Style) + sizeof(ObjectHeader)));
	oh4->class_num = 8;
	oh4->c_type = 1;
	Style* stl = (Style*) (oh4 + 1);
	stl->flagsoptions = htonl(10);

	//Setting the RSVP checksum.
	head->checksum = 0;
	head->checksum = RsvpChecksum(packet->data(), packetsize);
	Packet * p2 = this->IpEncap(packet, 46, m_localIP, this->m_next_hop, false);
	return p2;
}

Packet * RsvpDaemon::GenResvConf(IPAddress ip_of_receiver_that_requested_conf)
{
	int tailroom = 0;
	int packetsize = sizeof(CommonHeader) + 5 * sizeof(ObjectHeader) + sizeof(SessionClass) + sizeof(ErrorSpec)
	        + sizeof(ResvConfirm) + sizeof(Style) + sizeof(FilterSpec);

	int headroom = 0;
	WritablePacket *packet = Packet::make(headroom, 0, packetsize, tailroom);
	memset(packet->data(), 0, packet->length());

	//Fill in the common header
	CommonHeader* head = (CommonHeader*) (packet->data());

	head->verflags |= 1 << 4;
	head->msg_type = 7;	// 7 = Confirmation
	head->checksum = 0;	//0 for calculation purpose, fill in at end
	head->send_ttl = 30; //Should be the same as the one we just received, However, here we originate the path message.
	head->reserved = 0;
	head->Rsvp_length = htons(packetsize);

	//Now the first message header
	ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
	oh1->length = htons((uint16_t) (sizeof(SessionClass) + sizeof(ObjectHeader)));
	oh1->class_num = 1;
	oh1->c_type = 1;

	//followed by a Session
	SessionClass* session = (SessionClass*) (oh1 + 1);
	session->dstAddress = this->m_Sender_Session_DST;
	session->protocolId = this->m_Sender_Session_Protocol;	//
	session->flags = 0;
	session->dstPort = htons(this->m_Sender_Session_DSTPORT);

	//Second object header
	ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
	oh2->length = htons((uint16_t) (sizeof(ErrorSpec) + sizeof(ObjectHeader)));
	oh2->class_num = 6;
	oh2->c_type = 1;
	//ErrorSpec object
	ErrorSpec* es = (ErrorSpec*) (oh2 + 1);
	es->errorCode = 0;
	es->errorNodeAddress = m_localIP;
	es->errorValue = 0;
	es->flags = 0;

	//Third object header
	ObjectHeader * oh3 = (ObjectHeader*) (es + 1);
	oh3->length = htons((uint16_t) (sizeof(ResvConfirm) + sizeof(ObjectHeader)));
	oh3->class_num = 15;
	oh3->c_type = 1;
	//ErrorSpec object
	ResvConfirm* rc = (ResvConfirm*) (oh3 + 1);
	rc->receiverAddress = ip_of_receiver_that_requested_conf;

	//Fourth Object Header
	ObjectHeader * oh4 = (ObjectHeader*) (rc + 1);
	oh4->length = htons((uint16_t) (sizeof(Style) + sizeof(ObjectHeader)));
	oh4->class_num = 8;
	oh4->c_type = 1;
	Style* stl = (Style*) (oh4 + 1);
	stl->flagsoptions = htonl(10);

	ObjectHeader * oh5 = (ObjectHeader*) (stl + 1);
	oh5->length = htons((uint16_t) (sizeof(FilterSpec) + sizeof(ObjectHeader)));
	oh5->class_num = 10;
	oh5->c_type = 1;
	FilterSpec* fs = (FilterSpec*) (oh5 + 1);
	fs->srcAdress = m_localIP;
	fs->srcPort = this->m_packet_source_port;
	fs->voidbits = 0;

	//Setting the RSVP checksum.
	head->checksum = RsvpChecksum(packet->data(), packetsize);
	Packet * packet2 = this->IpEncap(packet, 46, m_localIP, this->m_next_hop, false);
	return packet2;
}

void RsvpDaemon::InitRsvp(IPAddress packet_src, IPAddress packet_dst, uint16_t s_port, uint16_t d_port)
{
	// The only time we enter this, is when the sender wants to start RSVP with
	// The packet being one of the UDP sources, so we can copy the IP'S
	this->m_rsvp_started = true;
	Packet * pathinit = this->GenPath(packet_src, s_port, packet_dst, d_port);
	Packet * ippath = this->IpEncap(pathinit, 46, packet_src, packet_dst, false);
	output(0).push(ippath);

}

void RsvpDaemon::SenderInitTearDown()
{

	Packet * pathtear = this->GenPathTear();
	this->m_path_set = false;
	//Path tear needs to shutdown the current session.
	if (m_verbose) {
		click_chatter("[%s] Pushing out a Path Tear.", inet_ntoa(m_localIP));
	}
	output(0).push(pathtear);

}

void RsvpDaemon::ReceiverInitTearDown()
{
	Packet * resvTear = this->GenResvTear();
	if (m_verbose) {
		click_chatter("[%s] Initiating ResvTear. Pushing out a ResvTear.", inet_ntoa(m_localIP));
	}
	output(0).push(resvTear);
}

Packet * RsvpDaemon::IpEncap(Packet* p, int proto, IPAddress src, IPAddress dst, bool RAP)
{
	/*
	 * This implementation of IPEncap was taken from an original implementation supplied by Gitte.
	 * After multiple questions to Johan Bergs, I was unable to integrate the click element and make use of
	 * its methods from within my own Element, and I could not possibly copy all the code used by IPEncap itself.
	 * not want to duplicate all the code.
	 *
	 * Code has been adapted to fit the needs of the project, but can be produced if required.
	 */
	int packetsize = p->length();
	int headroom = sizeof(click_ip) + sizeof(click_ether);
	if (RAP) {
		headroom += sizeof(RouterAlertIP);
	}
	if (WritablePacket* packet = Packet::make(headroom, 0, packetsize, 0)) {
		memcpy(packet->data(), p->data(), packetsize);
		if (packet = packet->push(sizeof(click_ip))) {	//Returns 0 if fails
			memset(packet->data(), 0, sizeof(click_ip));	//Alles v/d ipheader op 0
			click_ip* iph = (click_ip*) packet->data();
			iph->ip_v = 4;
			iph->ip_hl = 5;
			iph->ip_tos = 0;
			iph->ip_len = htons(sizeof(click_ip) + packetsize);
			iph->ip_ttl = 250;
			iph->ip_p = proto;
			iph->ip_src = src;
			iph->ip_dst = dst;
			iph->ip_id = htons(m_ip_id_count);
			iph->ip_sum = 0; //For CS calculation Purposes.
			m_ip_id_count++;
			if (RAP) {
				RouterAlertIP * raip = (RouterAlertIP *) (iph + 1);
				raip->value = 0;
			}
			iph->ip_sum = click_in_cksum((unsigned char*) packet->data(), packet->length());

			packet->set_network_header((unsigned char *) iph, sizeof(click_ip));
			//Tell Click to set the Network Header pointer,
			//otherwise IPGW fails the packet->has_transport_header() assert.
			p->kill();
			packet->set_dst_ip_anno(iph->ip_dst);
			return packet;
		}
	}
	return 0;
}

void RsvpDaemon::push(int int1, Packet* p)
{
	WritablePacket *q = p->uniqueify();

	click_ip * packet_ip_header = q->ip_header();
	in_addr packet_src = packet_ip_header->ip_src;
	in_addr packet_dst = packet_ip_header->ip_dst;

	uint16_t s_port = 0;
	uint16_t d_port = 0;

	if (q->has_transport_header()) {
		click_udp * packet_udp_header = q->udp_header();
		s_port = ntohs(packet_udp_header->uh_sport);
		d_port = ntohs(packet_udp_header->uh_dport);
	}

	if (m_first_time) {
		//We need to determine whether we are the sender of the stream or not
		//We do so by checking whether the src of the packets that enter equal ours
		//If it does, then we are the originator and we start RSVP.
		m_first_time = false;
		if (m_localIP == packet_src) {
			if (m_verbose) {
				click_chatter("[%s] I'm the host that's originating the Stream.", inet_ntoa(m_localIP));
				click_chatter("[%s] And I will be giving priority to packets with DST_PORT: %d",
				        inet_ntoa(m_localIP), d_port);
				click_chatter("[%s] I will be the one that sets up RSVP.", inet_ntoa(m_localIP));
				click_chatter("==========");
			}

			//This element is the source;
			this->m_packet_source_address = packet_src;
			this->m_packet_source_port = s_port;
			this->m_packet_dst_port = d_port;
			this->m_packet_dst_address = packet_dst;

			this->m_Sender_Session_DST = packet_dst;
			this->m_Sender_Session_Protocol = packet_ip_header->ip_p;
			this->m_Sender_Session_DSTPORT = d_port;

			m_RSVP_Sender = true;
			m_path_set = true;

		} else if (m_localIP == packet_dst) {
			if (m_verbose) {
				click_chatter("[%s] I'm the Receiver!", inet_ntoa(m_localIP));
				click_chatter("==========");
			}
			m_RSVP_Receiver = true;
		} else {
			if (m_verbose) {
				click_chatter("[%s] I'm a Router!", inet_ntoa(m_localIP));
				click_chatter("==========");
			}

		}
	}

	if (packet_ip_header->ip_p == 46) {
		//RSVP Clause.

		CommonHeader* head = reinterpret_cast<CommonHeader*>(packet_ip_header + 1);
		uint8_t mtype = head->msg_type;

		if (mtype == 1) {
			//PATH MESSAGE

			ObjectHeader* oh1 = reinterpret_cast<ObjectHeader*>(head + 1);
			SessionClass* session = (SessionClass*) (oh1 + 1);
			ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
			RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
			ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
			TimeValues * tv = (TimeValues*) (oh3 + 1);
			ObjectHeader * oh4 = (ObjectHeader*) (tv + 1);
			RsvpSenderTemplate* rstemplate = (RsvpSenderTemplate*) (oh4 + 1);
			ObjectHeader * oh5 = (ObjectHeader*) (rstemplate + 1);
			RsvpSenderTSpec * rstspec = (RsvpSenderTSpec *) (oh5 + 1);
			if (m_localIP == packet_dst) {
				if (m_verbose) {
					click_chatter("[%s] I've received a Path Message, IP Routed to me!",
					        inet_ntoa(m_localIP));
				}
				//Path Message has reached its end destination
				//Handle logic and send a generated RESV message
				Packet * ip_resv_resp_packet;
				if (this->PsVector.size() == 0) {
					this->m_path_state.prev_hop_node = hc->hopAdress;
					this->m_path_state.stemplate = (*rstemplate);
					this->m_path_state.session = (*session);
					this->m_path_state.ts = (*rstspec);
					this->m_path_state.path_state_set = true;
					this->m_path_state.L = (uint32_t) (ntohl(tv->R) * 1.5 * 3.5);
					this->m_local_state_lifetime = (double) (ntohl(tv->R) * 1.5 * 3.5);
					m_path_set = true;
					PsVector.push_back(this->m_path_state);

					if (m_verbose) {
						click_chatter("[%s] Creating a PATH state for a new Session!",
						        inet_ntoa(m_localIP));
						click_chatter("[%s] Responding with a Reservation Message!",
						        inet_ntoa(m_localIP));
					}

					IPAddress sessiondst = session->dstAddress;
					uint16_t sessiondstport = ntohs(session->dstPort);
					Packet * resv_resp_packet = this->GenResv(sessiondst, sessiondstport,
					        this->m_path_state.session.protocolId,
					        ntohs(this->m_path_state.stemplate.srcPort),
					        this->m_path_state.stemplate.srcAdress);
					ip_resv_resp_packet = this->IpEncap(resv_resp_packet, 46, m_localIP,
					        m_path_state.prev_hop_node, false);
					resv_resp_packet->kill();
					//The Receiver is now idle after sending a RESV, and waits until its path state might time out.
					if (m_verbose) {
						click_chatter(
						        "[%s] I will now stay idle until my Path State times out, after: %f ms.",
						        inet_ntoa(m_localIP), this->m_local_state_lifetime);
						click_chatter("==========");
					}
					this->m_PathInit_timer.reschedule_after_msec(this->m_local_state_lifetime);
					/*
					 * Uncomment the following and another one a little down to
					 * Generate a RESV Tear!
					 *
					 *
					 *
					 */
					//this->m_PathInit_timer.reschedule_after_msec(5000);
				} else {
					bool found = false;
					for (Vector<PathState>::iterator it = PsVector.begin(); it != PsVector.end();
					        it++) {
						if ((*it).session.dstAddress == (*session).dstAddress
						        && (*it).session.dstPort == (*session).dstPort
						        && (*it).session.protocolId == (*session).protocolId) {
							found = true;
							if (m_verbose) {
								click_chatter(
								        "[%s] Received a Path State Update for a current live Session!",
								        inet_ntoa(m_localIP));
								click_chatter(
								        "[%s] Responding with a Reservation Message.",
								        inet_ntoa(m_localIP));
							}
							IPAddress sessiondst = session->dstAddress;
							uint16_t sessiondstport = ntohs(session->dstPort);
							Packet * resv_resp_packet = this->GenResv(sessiondst,
							        sessiondstport, this->m_path_state.session.protocolId,
							        ntohs(this->m_path_state.stemplate.srcPort),
							        this->m_path_state.stemplate.srcAdress);
							ip_resv_resp_packet = this->IpEncap(resv_resp_packet, 46,
							        m_localIP, m_path_state.prev_hop_node, false);
							resv_resp_packet->kill();
							//The router or Receiver is now idle after sending a RESV, and waits until its path state might time out.
							if (m_verbose) {
								click_chatter(
								        "[%s] I'm now idle for %f ms, until my Lifetime runs out.",
								        inet_ntoa(m_localIP),
								        this->m_local_state_lifetime);
								click_chatter("==========");
							}
							this->m_PathInit_timer.reschedule_after_msec(
							        this->m_local_state_lifetime);

							/*
							 * Uncomment the following and another one a little down to
							 * Generate a RESV Tear!
							 *
							 *
							 *
							 */

							//this->m_PathInit_timer.reschedule_after_msec(5000);
						}
					}
					if (!found) {

						this->m_path_state.prev_hop_node = hc->hopAdress;
						this->m_path_state.stemplate = (*rstemplate);
						this->m_path_state.session = (*session);
						this->m_path_state.ts = (*rstspec);
						this->m_path_state.path_state_set = true;
						this->m_path_state.L = (uint32_t) (ntohl(tv->R) * 1.5 * 3.5);
						this->m_local_state_lifetime = this->m_path_state.L;
						m_path_set = true;
						PsVector.push_back(this->m_path_state);
						if (m_verbose) {
							click_chatter(
							        "[%s] I'm not holding a Path State for this session yet!",
							        inet_ntoa(m_localIP));
							click_chatter("[%s] Creating a PATH state for a new Session!",
							        inet_ntoa(m_localIP));
						}
						IPAddress sessiondst = session->dstAddress;
						uint16_t sessiondstport = ntohs(session->dstPort);
						Packet * resv_resp_packet = this->GenResv(sessiondst, sessiondstport,
						        this->m_path_state.session.protocolId,
						        ntohs(this->m_path_state.stemplate.srcPort),
						        this->m_path_state.stemplate.srcAdress);
						ip_resv_resp_packet = this->IpEncap(resv_resp_packet, 46, m_localIP,
						        m_path_state.prev_hop_node, false);
						resv_resp_packet->kill();
						//The router or Receiver is now idle after sending a RESV, and waits until its path state might time out.
						if (m_verbose) {
							click_chatter(
							        "[%s] I'm now idle for %f ms, until my lifetime runs out.",
							        inet_ntoa(m_localIP), this->m_local_state_lifetime);
							click_chatter("==========");
						}
						this->m_PathInit_timer.reschedule_after_msec(
						        this->m_local_state_lifetime);
					}
					//Check whether the PATH still corresponds to the current reservation, and update the soft state.

				}

				/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				 *% 		ERROR CHECKING 				%
				 *%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				 */
				uint8_t ec = 0;
				uint16_t ev = 0;
				bool iserror = false;
				//This error checking works fine...
				if (oh1->c_type != 1 || (oh1->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in PATH: First header.",
						        inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh1->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh1->class_num;
					ev = ev << 8;
					ev += oh1->c_type;
					//Generate Path Error
					Packet * error = this->GenPathErr(ec, ev);
					output(0).push(error);
				}
				if (oh2->c_type != 1 || (oh2->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in PATH: Second Header.",
						        inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh2->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh2->class_num;
					ev = ev << 8;
					ev += oh2->c_type;
					//Generate Path Error
					Packet * error = this->GenPathErr(ec, ev);
					output(0).push(error);
				}
				if (oh3->c_type != 1 || (oh3->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in PATH. Third Header.",
						        inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh3->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh3->class_num;
					ev = ev << 8;
					ev += oh1->c_type;
					//Generate Path Error
					Packet * error = this->GenPathErr(ec, ev);
					output(0).push(error);

				}
				if (oh4->c_type != 1 || (oh4->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in PATH. Fourth Header.",
						        inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh4->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh4->class_num;
					ev = ev << 8;
					ev += oh4->c_type;
					//Generate Path Error
					Packet * error = this->GenPathErr(ec, ev);
					output(0).push(error);
				}
				if (oh5->c_type != 2 || (oh5->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in PATH. Header Five.",
						        inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh5->c_type != 2) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh5->class_num;
					ev = ev << 8;
					ev += oh5->c_type;
					//Generate Path Error
					Packet * error = this->GenPathErr(ec, ev);
					output(0).push(error);
				}

				if (!iserror) {
					//Check for 23 error. //RSVP SYSTEM ERROR
					ec = 23;
					if (ntohs(head->Rsvp_length) != PATH_SIZE) {
						if (m_verbose) {
							click_chatter("[%s] Length is: %d, should be: %d.",
							        inet_ntoa(m_localIP), ntohs(head->Rsvp_length),
							        PATH_SIZE);
						}
						iserror = true;
						ev = 1;
						Packet * error = this->GenPathErr(ec, ev);
						output(0).push(error);
						//EV = 1 is WRONG RSVP HEADER LENGTH
					} else if (head->verflags != 16) {
						if (m_verbose) {
							click_chatter("[%s] Version is: %d, should be: %d.",
							        inet_ntoa(m_localIP), head->verflags, 16);
						}
						iserror = true;
						ev = 2;
						Packet * error = this->GenPathErr(ec, ev);
						output(0).push(error);
					} else if (head->msg_type > 7) {
						if (m_verbose) {
							click_chatter(
							        "[%s] Message type is: %d, should be between 1 and 7",
							        inet_ntoa(m_localIP), head->msg_type);
						}
						iserror = true;
						ev = 3;
						Packet * error = this->GenPathErr(ec, ev);
						output(0).push(error);

//					} else if (ntohs(head->checksum)
//					        != correct_cs) {
//						click_chatter("Checksum is: %d, should be: %d.", ntohs(head->checksum),
//						        click_in_cksum((unsigned char *) head, PATH_SIZE));
//						iserror = true;
//						ev = 4;
//						Packet * error = this->GenPathErr(ec, ev);
//						output(0).push(error);
					} else if (ntohs(head->Rsvp_length) % 4 != 0 || ntohs(head->Rsvp_length) < 4) {
						if (m_verbose) {
							click_chatter("[%s] Illegal object length",
							        inet_ntoa(m_localIP));
						}
						iserror = true;
						ev = 5;
						Packet * error = this->GenPathErr(ec, ev);
						output(0).push(error);
					} else if (oh1->class_num != 1 || oh2->class_num != 3 || oh3->class_num != 5
					        || oh4->class_num != 11 || oh5->class_num != 12) {
						if (m_verbose) {
							click_chatter("[%s] Illegal formation of PATH Message.",
							        inet_ntoa(m_localIP));
						}
						iserror = true;
						ev = 6;
						Packet * error = this->GenPathErr(ec, ev);
						output(0).push(error);
					} else if (ntohs(rstemplate->srcPort) != 0 && ntohs(session->dstPort) == 0) {
						click_chatter("[%s] Discovered a Port conflict.", inet_ntoa(m_localIP));
						iserror = true;
						ev = 7;
						Packet * error = this->GenPathErr(ec, ev);
						output(0).push(error);
					}
				}
				//q->kill();

				if (!iserror) {
					output(0).push(ip_resv_resp_packet);
				}

			} else {
				//ROUTER RECEIVING PATH
				if (m_verbose) {
					click_chatter("[%s] I've received a Path Message, Routing to the receiver!",
					        inet_ntoa(m_localIP));
				}
				//We are just a router along the path.
				//Save the path state
				if (PsVector.size() == 0) {
					if (m_verbose) {
						click_chatter("[%s] Generating a new PATH state for a new Session.",
						        inet_ntoa(m_localIP));
					}
					this->m_path_state.prev_hop_node = hc->hopAdress;
					this->m_path_state.path_state_set = true;
					this->m_path_state.stemplate = (*rstemplate);
					this->m_path_state.session = (*session);
					this->m_path_state.ts = (*rstspec);
					this->m_path_state.L = (uint32_t) (ntohl(tv->R) * 1.5 * 3.5);
					this->m_local_state_lifetime = this->m_path_state.L;
					PsVector.push_back(this->m_path_state);
					m_path_set = true;
					hc->hopAdress = m_localIP;
					head->checksum = 0;
					head->checksum = click_in_cksum((unsigned char *) head, PATH_SIZE);
					//The router or Receiver is now idle after sending a RESV, and waits until its path state might time out.
					if (m_verbose) {
						click_chatter("[%s] I'm now idle for %f ms, until my lifetime expires.",
						        inet_ntoa(m_localIP), this->m_local_state_lifetime);
					}
					this->m_PathInit_timer.reschedule_after_msec(this->m_local_state_lifetime);
				} else {
					bool found = false;
					for (Vector<PathState>::iterator it = PsVector.begin(); it != PsVector.end();
					        it++) {
						if ((*it).session.dstAddress == (*session).dstAddress
						        && (*it).session.dstPort == (*session).dstPort
						        && (*it).session.protocolId == (*session).protocolId) {
							if (m_verbose) {
								click_chatter(
								        "[%s] Received a Path State Update for a current live Session!",
								        inet_ntoa(m_localIP));
							}
							this->m_PathInit_timer.reschedule_after_msec((*it).L);
							found = true;
							hc->hopAdress = m_localIP;
							head->checksum = 0;
							head->checksum = click_in_cksum((unsigned char *) head,
							PATH_SIZE);
							//The router or Receiver is now idle after sending a RESV, and waits until its path state might time out.
							if (m_verbose) {
								click_chatter("[%s] I'm now idle for %f ms.",
								        inet_ntoa(m_localIP),
								        this->m_local_state_lifetime);
							}
						}
					}
					if (!found) {
						if (m_verbose) {
							click_chatter(
							        "[%s] Unable to find a Path state for this Session.",
							        inet_ntoa(m_localIP));
							click_chatter("[%s] Creating a new Path State.",
							        inet_ntoa(m_localIP));
						}
						this->m_path_state.prev_hop_node = hc->hopAdress;
						this->m_path_state.path_state_set = true;
						this->m_path_state.stemplate = (*rstemplate);
						this->m_path_state.session = (*session);
						this->m_path_state.ts = (*rstspec);
						this->m_path_state.L = (uint32_t) (ntohl(tv->R) * 1.5 * 3.5);
						PsVector.push_back(this->m_path_state);
						m_path_set = true;
						this->m_local_state_lifetime = (uint32_t) (ntohl(tv->R) * 1.5 * 3.5);
						hc->hopAdress = m_localIP;
						head->checksum = 0;
						head->checksum = click_in_cksum((unsigned char *) head, PATH_SIZE);
						//The router or Receiver is now idle after sending a RESV, and waits until its path state might time out.
						if (m_verbose) {
							click_chatter(
							        "[%s] I'm now idle for %f ms, until my lifetime expires.",
							        inet_ntoa(m_localIP), this->m_local_state_lifetime);
						}
						this->m_PathInit_timer.reschedule_after_msec(
						        this->m_local_state_lifetime);
					}
				}
				/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				 *% 		ERROR CHECKING 				%
				 *%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				 */
				uint8_t ec = 0;
				uint16_t ev = 0;
				bool iserror = false;
				if (oh1->c_type != 1 || (oh1->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in PATH. Header 1",
						        inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh1->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh1->class_num;
					ev = ev << 8;
					ev += oh1->c_type;
					//Generate Path Error
					Packet * error = this->GenPathErr(ec, ev);
					output(0).push(error);

				}
				if (oh2->c_type != 1 || (oh2->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in PATH. Header 2",
						        inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh2->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh2->class_num;
					ev = ev << 8;
					ev += oh2->c_type;
					//Generate Path Error
					Packet * error = this->GenPathErr(ec, ev);
					output(0).push(error);
				}
				if (oh3->c_type != 1 || (oh3->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in PATH. Header 3",
						        inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh3->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh3->class_num;
					ev = ev << 8;
					ev += oh3->c_type;
					//Generate Path Error
					Packet * error = this->GenPathErr(ec, ev);
					output(0).push(error);

				}
				if (oh4->c_type != 1 || (oh4->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in PATH. Header 4",
						        inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh4->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh4->class_num;
					ev = ev << 8;
					ev += oh4->c_type;
					//Generate Path Error
					Packet * error = this->GenPathErr(ec, ev);
					output(0).push(error);
				}
				if (oh5->c_type != 2 || (oh5->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in PATH. Header 5",
						        inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh5->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh5->class_num;
					ev = ev << 8;
					ev += oh5->c_type;
					//Generate Path Error
					Packet * error = this->GenPathErr(ec, ev);
					output(0).push(error);
				}
				if (!iserror) {
					//RSVP SYSTEM ERROR
					ec = 23;
					if (ntohs(head->Rsvp_length) != PATH_SIZE) {
						if (m_verbose) {
							click_chatter("[%s] Length is: %d, should be: %d.",
							        inet_ntoa(m_localIP), ntohs(head->Rsvp_length),
							        PATH_SIZE);
						}
						iserror = true;
						ev = 1;
						//EV = 1 is WRONG RSVP HEADER LENGTH
						Packet * error = this->GenPathErr(ec, ev);
						output(0).push(error);
					} else if (head->verflags != 16) {
						if (m_verbose) {
							click_chatter("[%s] Version is: %d, should be: %d.",
							        inet_ntoa(m_localIP), head->verflags, 16);
						}
						iserror = true;
						ev = 2;
						Packet * error = this->GenPathErr(ec, ev);
						output(0).push(error);
					} else if (head->msg_type > 7) {
						if (m_verbose) {
							click_chatter(
							        "[%s] Message type is: %d, should be between 1 and 7",
							        inet_ntoa(m_localIP), head->msg_type);
						}
						iserror = true;
						ev = 3;
						Packet * error = this->GenPathErr(ec, ev);
						output(0).push(error);
					}
					//					else if(ntohs(head->checksum) != (click_in_cksum((unsigned char *)head, PATH_SIZE))){
					//						click_chatter("Length is: %d, should be: %d.", ntohs(head->checksum), click_in_cksum((unsigned char *) head, PATH_SIZE) );
					//						iserror = true;
					//						ev = 4;
					//				}
					else if (ntohs(head->Rsvp_length) % 4 != 0 || ntohs(head->Rsvp_length) < 4) {
						if (m_verbose) {
							click_chatter("[%s] Illegal object length",
							        inet_ntoa(m_localIP));
						}
						iserror = true;
						ev = 5;
						Packet * error = this->GenPathErr(ec, ev);
						output(0).push(error);
					} else if (oh1->class_num != 1 || oh2->class_num != 3 || oh3->class_num != 5
					        || oh4->class_num != 11 || oh5->class_num != 12) {
						if (m_verbose) {
							click_chatter("[%s] Illegal formation of PATH Message.",
							        inet_ntoa(m_localIP));
						}
						iserror = true;
						ev = 6;
						Packet * error = this->GenPathErr(ec, ev);
						output(0).push(error);
					} else if (ntohs(rstemplate->srcPort) != 0 && ntohs(session->dstPort) == 0) {
						if (m_verbose) {
							click_chatter("[%s] Port conflict.", inet_ntoa(m_localIP));
						}
						iserror = true;
						ev = 7;
						Packet * error = this->GenPathErr(ec, ev);
						output(0).push(error);
					}
				}

				//Push the packet further into the network.
				if (!iserror) {
					output(3).push(q);
				}
			}
		} else if (mtype == 2) {
			ObjectHeader* oh1 = reinterpret_cast<ObjectHeader*>(head + 1);
			SessionClass* session = (SessionClass*) (oh1 + 1);
			ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
			RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
			ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
			TimeValues * tv = (TimeValues *) (oh3 + 1);
			ObjectHeader * oh4 = (ObjectHeader*) (tv + 1);
			Style * st = (Style *) (oh4 + 1);
			ObjectHeader * oh5 = (ObjectHeader*) (st + 1);
			RsvpFlowSpec * rfs = (RsvpFlowSpec *) (oh5 + 1);
			ObjectHeader * oh6 = (ObjectHeader*) (rfs + 1);
			FilterSpec * filter = (FilterSpec *) (oh6 + 1);

			if (m_RSVP_Sender) {
				if (m_verbose) {
					click_chatter("[%s] Received a RESV. I'm the sending Host.",
					        inet_ntoa(m_localIP));
				}

				//RESV Message has reached its end destination
				//Handle logic and start Priority.
				bool found = false;
				for (Vector<PathState>::iterator it = PsVector.begin(); it != PsVector.end(); it++) {
					if ((*it).session.dstAddress == (*session).dstAddress
					        && (*it).session.dstPort == (*session).dstPort
					        && (*it).session.protocolId == (*session).protocolId) {
						if (m_verbose) {
							click_chatter(
							        "[%s] Received a Reservation for a current live Session!",
							        inet_ntoa(m_localIP));
						}
						found = true;
						m_resv_set = true;
						m_next_hop = packet_ip_header->ip_src;
						(*it).next_hop_node = packet_ip_header->ip_src;
						it->resv_state_set = true;
						m_give_prio = true;
						m_rsvp_setup_complete = true;
						m_resv_set = true;
						this->m_next_hop = packet_ip_header->ip_src;
						//Change the destination of the IP to that of the prev hop, and route it through
					}
				}

				//Handle RESV ERRORS

				uint8_t ec = 0;
				uint16_t ev = 0;
				bool iserror = false;
				if (oh1->c_type != 1 || (oh1->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in Resv.", inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh1->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh1->class_num;
					ev = ev << 8;
					ev += oh1->c_type;
					//Generate Resv Error
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}
				if (oh2->c_type != 1 || (oh2->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in Resv.", inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh2->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh2->class_num;
					ev = ev << 8;
					ev += oh2->c_type;
					//Generate Resv Error
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}
				if (oh3->c_type != 1 || (oh3->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in Resv.", inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh3->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh3->class_num;
					ev = ev << 8;
					ev += oh3->c_type;
					//Generate Resv Error
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);

				}
				if (oh4->c_type != 1 || (oh4->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in Resv.", inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh4->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh4->class_num;
					ev = ev << 8;
					ev += oh4->c_type;
					//Generate Resv Error
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}
				if (oh5->c_type != 2 || (oh5->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in Resv.", inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh5->c_type != 2) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh5->class_num;
					ev = ev << 8;
					ev += oh5->c_type;
					//Generate Resv Error
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}
				if (oh6->c_type != 1 || (oh6->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in Resv.", inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh6->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh6->class_num;
					ev = ev << 8;
					ev += oh6->c_type;
					//Generate Resv Error
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}
				ec = 6;
				if (st->flagsoptions != htonl(10)) {
					if (m_verbose) {
						click_chatter("[%s] Unknown Reservation Style.", inet_ntoa(m_localIP));
					}
					iserror = true;
					ev = 0;
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}

				ec = 23;
				if (ntohs(head->Rsvp_length) != RESV_SIZE) {
					if (m_verbose) {
						click_chatter("[%s] Length is: %d, should be: %d.", inet_ntoa(m_localIP), ntohs(head->Rsvp_length),
							RESV_SIZE);
					}

					iserror = true;
					ev = 1;
					//EV = 1 is WRONG RSVP HEADER LENGTH
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				} else if (head->verflags != 16) {
					if (m_verbose) {
						click_chatter("[%s] Version is: %d, should be: %d.", inet_ntoa(m_localIP), head->verflags, 16);
					}
					iserror = true;
					ev = 2;
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				} else if (head->msg_type > 7) {
					if (m_verbose) {
						click_chatter("[%s] Message type is: %d, should be between 1 and 7", inet_ntoa(m_localIP), head->msg_type);
					}
					iserror = true;
					ev = 3;
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}
				//					else if(ntohs(head->checksum) != (click_in_cksum((unsigned char *)head, PATH_SIZE))){
				//						click_chatter("Length is: %d, should be: %d.", ntohs(head->checksum), click_in_cksum((unsigned char *) head, PATH_SIZE) );
				//						iserror = true;
				//						ev = 4;
				//					}
				else if (ntohs(head->Rsvp_length) % 4 != 0 || ntohs(head->Rsvp_length) < 4) {
					if (m_verbose) {
						click_chatter("[%s] Illegal object length", inet_ntoa(m_localIP));
					}
					iserror = true;
					ev = 5;
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				} else if (oh1->class_num != 1 || oh2->class_num != 3 || oh3->class_num != 5
				        || oh4->class_num != 8 || oh5->class_num != 9 || oh6->class_num != 10) {
					if (m_verbose) {
						click_chatter("[%s] Illegal formation of RESV Message.", inet_ntoa(m_localIP));
					}
					iserror = true;
					ev = 6;
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}
				if(iserror){
					m_give_prio = false;
					m_rsvp_setup_complete = false;
				}
				q->kill();				//Kill it because it's at the end of the line.

			} else {
				//Router receiving a RESV
				m_give_prio = true;
				m_rsvp_setup_complete = true;
				m_resv_set = true;
				this->m_next_hop = packet_ip_header->ip_src;

				uint8_t ec = 0;
				uint16_t ev = 0;
				bool iserror = false;
				if (oh1->c_type != 1 || (oh1->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in Resv.", inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh1->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh1->class_num;
					ev = ev << 8;
					ev += oh1->c_type;
					//Generate Path Error
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}
				if (oh2->c_type != 1 || (oh2->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in Resv.", inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh2->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh2->class_num;
					ev = ev << 8;
					ev += oh2->c_type;
					//Generate Path Error
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}
				if (oh3->c_type != 1 || (oh3->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in Resv.", inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh3->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh3->class_num;
					ev = ev << 8;
					ev += oh3->c_type;
					//Generate Path Error
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);

				}
				if (oh4->c_type != 1 || (oh4->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in Resv.", inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh4->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh4->class_num;
					ev = ev << 8;
					ev += oh4->c_type;
					//Generate Path Error
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}
				if (oh5->c_type != 2 || (oh5->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in Resv.", inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh5->c_type != 2) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh5->class_num;
					ev = ev << 8;
					ev += oh5->c_type;
					//Generate Path Error
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}
				if (oh6->c_type != 1 || (oh6->class_num > 15)) {
					if (m_verbose) {
						click_chatter("[%s] Found an Error in Resv.", inet_ntoa(m_localIP));
					}
					iserror = true;
					if (oh6->c_type != 1) {
						ec = 14;
					} else {
						ec = 13;
					}
					ev = oh6->class_num;
					ev = ev << 8;
					ev += oh6->c_type;
					//Generate Path Error
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}
				ec = 6;
				if (st->flagsoptions != htonl(10)) {
					if (m_verbose) {
						click_chatter("[%s] Unknown Reservation Style.", inet_ntoa(m_localIP));
					}
					iserror = true;
					ev = 0;
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}

				ec = 23;
				if (ntohs(head->Rsvp_length) != RESV_SIZE) {
					if (m_verbose) {
						click_chatter("[%s] Length is: %d, should be: %d.", inet_ntoa(m_localIP), ntohs(head->Rsvp_length),
							RESV_SIZE);
					}

					iserror = true;
					ev = 1;
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				} else if (head->verflags != 16) {
					if (m_verbose) {
						click_chatter("[%s] Version is: %d, should be: %d.", inet_ntoa(m_localIP), head->verflags, 16);
					}
					iserror = true;
					ev = 2;
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				} else if (head->msg_type > 7) {
					if (m_verbose) {
						click_chatter("[%s] Message type is: %d, should be between 1 and 7", inet_ntoa(m_localIP), head->msg_type);
					}
					iserror = true;
					ev = 3;
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}
//					else if(ntohs(head->checksum) != (click_in_cksum((unsigned char *)head, PATH_SIZE))){
//						click_chatter("Length is: %d, should be: %d.", ntohs(head->checksum), click_in_cksum((unsigned char *) head, PATH_SIZE) );
//						iserror = true;
//						ev = 4;
//					}
				else if (ntohs(head->Rsvp_length) % 4 != 0 || ntohs(head->Rsvp_length) < 4) {
					if (m_verbose) {
						click_chatter("[%s] Illegal object length", inet_ntoa(m_localIP));
					}
					iserror = true;
					ev = 5;
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				} else if (oh1->class_num != 1 || oh2->class_num != 3 || oh3->class_num != 5
				        || oh4->class_num != 8 || oh5->class_num != 9 || oh6->class_num != 10) {
					if (m_verbose) {
						click_chatter("[%s] Illegal formation of RESV Message.", inet_ntoa(m_localIP));
					}
					iserror = true;
					ev = 6;
					Packet * error = this->GenResvErr(ec, ev);
					output(0).push(error);
				}

				bool found = false;
				for (Vector<PathState>::iterator it = PsVector.begin(); it != PsVector.end(); it++) {
					if ((*it).session.dstAddress == (*session).dstAddress
					        && (*it).session.dstPort == (*session).dstPort
					        && (*it).session.protocolId == (*session).protocolId) {
						if (m_verbose) {
							click_chatter(
							        "[%s] Received a Reservation for a current live Session!",
							        inet_ntoa(m_localIP));
						}

						found = true;
						m_resv_set = true;
						m_next_hop = packet_ip_header->ip_src;
						(*it).next_hop_node = packet_ip_header->ip_src;
						it->resv_state_set = true;
						//Change the destination of the IP to that of the prev hop, and route it through
						hc->hopAdress = m_localIP;
						head->checksum = 0;
						head->checksum = click_in_cksum((unsigned char *) head, RESV_SIZE);
						packet_ip_header->ip_src = m_localIP;
						packet_ip_header->ip_dst = this->m_path_state.prev_hop_node;
						packet_ip_header->ip_sum = 0;
						packet_ip_header->ip_sum = click_in_cksum(
						        (unsigned char *) packet_ip_header,
						        sizeof(click_ip) + RESV_SIZE);
						this->m_PathInit_timer.reschedule_after_msec((*it).L);
					}
				}
				if (!found) {
					click_chatter("Didn't find a corresponding PATH.");
					q->kill();
					iserror = true;
				}
				//Check for possible reservation errors.
				if (!iserror) {
					output(3).push(q);
				}
			}
		} else if (mtype == 3) {
			//PATH ERROR LOGIC
			ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
			SessionClass* session = (SessionClass*) (oh1 + 1);
			ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
			ErrorSpec* es = (ErrorSpec*) (oh2 + 1);

			if (m_RSVP_Sender) {
				click_chatter("[%s] Found a PathErr, I am the sender.", inet_ntoa(m_localIP));
				//m_path_set = false;
				q->kill();
			} else {
				click_chatter("[%s] Found a PathErr, Sending it upwards.", inet_ntoa(m_localIP));
				//m_path_set = false;
				packet_ip_header->ip_src = m_localIP;
				packet_ip_header->ip_dst = this->m_path_state.prev_hop_node;
				packet_ip_header->ip_sum = 0;
				packet_ip_header->ip_sum = click_in_cksum((unsigned char *) packet_ip_header,
				        sizeof(click_ip) + PATH_ERR_SIZE);
				output(3).push(q);
			}
		} else if (mtype == 4) {
			//RESV ERROR
			ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
			SessionClass* session = (SessionClass*) (oh1 + 1);
			ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
			RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
			ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
			Style* s = (Style*) (oh3 + 1);
			ObjectHeader * oh4 = (ObjectHeader*) (s + 1);
			IPAddress t1;
			for (Vector<PathState>::iterator it = PsVector.begin(); it != PsVector.end(); it++) {
				if ((*it).session.dstAddress == (*session).dstAddress
				        && (*it).session.dstPort == (*session).dstPort
				        && (*it).session.protocolId == (*session).protocolId) {
					click_chatter("In RESVERR, looking for next hop...");
					t1 = it->next_hop_node;

				}
			}
			if (m_RSVP_Receiver) {
				click_chatter("[%s] Found a ResvErr, I am the Receiver.", inet_ntoa(m_localIP));
				//m_resv_set = false;
				//Packet reached the last step of the line, process and kill.
				q->kill();
			} else {
				click_chatter("[%s] Found a ResvErr, pushing it down.", inet_ntoa(m_localIP));
				//m_resv_set = false;
				hc->hopAdress = m_localIP;
				head->checksum = 0;
				head->checksum = click_in_cksum((unsigned char *) head, RESV_ERR_SIZE);

				packet_ip_header->ip_src = m_localIP;
				//packet_ip_header->ip_dst = this->PsVector[0].next_hop_node;//TODO Change for multiples...
				packet_ip_header->ip_dst = t1;
				packet_ip_header->ip_sum = 0;
				packet_ip_header->ip_sum = click_in_cksum((unsigned char *) packet_ip_header,
				        sizeof(click_ip) + RESV_ERR_SIZE);
				output(3).push(q);
			}
		} else if (mtype == 5) {
			//PathTear
			ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
			SessionClass* session = (SessionClass*) (oh1 + 1);
			ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
			RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
			ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
			RsvpSenderTemplate* rstemplate = (RsvpSenderTemplate*) (oh3 + 1);
			ObjectHeader * oh4 = (ObjectHeader*) (rstemplate + 1);
			RsvpSenderTSpec * rts = (RsvpSenderTSpec*) (oh4 + 1);

			if (m_localIP != packet_ip_header->ip_dst) {
				click_chatter("[%s] Found a PathTear, Sending it downstream.", inet_ntoa(m_localIP));
				if (this->CleanupPathState((*session), (*rstemplate))) {
					output(3).push(q);
					click_chatter("[%s] Deleting a live Path State.", inet_ntoa(m_localIP));
					m_path_set = false;
				} else {
					q->kill();
				}
			} else {
				click_chatter("[%s] PathTear reached end of line, killing.", inet_ntoa(m_localIP));
				if (this->CleanupPathState(*session, *rstemplate)) {
					m_path_set = false;
					click_chatter("[%s] Deleting a live Path State.", inet_ntoa(m_localIP));
					q->kill();
				} else {
					q->kill();
				}
			}
		} else if (mtype == 6) {
			ObjectHeader * oh1 = (ObjectHeader*) (head + 1);
			SessionClass* session = (SessionClass*) (oh1 + 1);
			ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
			RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
			ObjectHeader * oh3 = (ObjectHeader*) (hc + 1);
			Style* s = (Style*) (oh3 + 1);
			ObjectHeader * oh5 = (ObjectHeader*) (s + 1);
			FilterSpec* fis = (FilterSpec*) (oh5 + 1);
			Vector<PathState>::iterator del;
			bool found = false;
			IPAddress t1;
			for (Vector<PathState>::iterator it = PsVector.begin(); it != PsVector.end(); it++) {
				if ((*it).session.dstAddress == (*session).dstAddress
				        && (*it).session.dstPort == (*session).dstPort
				        && (*it).session.protocolId == (*session).protocolId) {
					t1 = it->prev_hop_node;
					it->next_hop_node = 0;
					it->resv_state_set = false;
				}
			}

			if (m_RSVP_Sender) {
				click_chatter("[%s] Received a ResvTear, at end of the line!", inet_ntoa(m_localIP));
				this->m_resv_set = false;
				this->m_next_hop = 0;
				q->kill();
			} else {
				click_chatter("[%s] Received a ResvTear, Sending it upstream!", inet_ntoa(m_localIP));
				ObjectHeader* oh1 = reinterpret_cast<ObjectHeader*>(head + 1);
				SessionClass* session = (SessionClass*) (oh1 + 1);
				ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
				RsvpHopClass* hc = (RsvpHopClass*) (oh2 + 1);
				//hc->hopAdress = this->m_path_state.prev_hop_node;
				hc->hopAdress = t1;
				head->checksum = 0;
				head->checksum = click_in_cksum((unsigned char *) head, RESV_TEAR_SIZE);
				packet_ip_header->ip_src = m_localIP;
				//packet_ip_header->ip_dst = this->m_path_state.prev_hop_node;
				packet_ip_header->ip_dst = t1;
				packet_ip_header->ip_sum = 0;
				packet_ip_header->ip_sum = click_in_cksum((unsigned char *) packet_ip_header,
				        sizeof(click_ip));

				this->m_resv_set = false;
				//this->m_path_set = false;//Should only do resv!
				output(3).push(q);
			}
		} else if (mtype == 7) {
			ObjectHeader* oh1 = reinterpret_cast<ObjectHeader*>(head + 1);
			SessionClass* session = (SessionClass*) (oh1 + 1);

			ObjectHeader * oh2 = (ObjectHeader*) (session + 1);
			ErrorSpec * es = (ErrorSpec*) (oh2 + 1);

			ObjectHeader * oh3 = (ObjectHeader*) (es + 1);
			ResvConfirm * rc = (ResvConfirm *) (oh3 + 1);

			ObjectHeader * oh4 = (ObjectHeader*) (rc + 1);
			Style * st = (Style*) (oh4 + 1);

			ObjectHeader * oh5 = (ObjectHeader*) (st + 1);
			FilterSpec * filter = (FilterSpec *) (oh5 + 1);

			if (m_RSVP_Receiver) {
				click_chatter("[%s] Received a Resvervation Confirmation, at end of the line!",
				        inet_ntoa(m_localIP));
				q->kill();
			} else {
				packet_ip_header->ip_src = m_localIP;
				packet_ip_header->ip_dst = this->m_next_hop;
				packet_ip_header->ip_sum = 0;
				packet_ip_header->ip_sum = click_in_cksum((unsigned char *) packet_ip_header,
				        sizeof(click_ip));
				output(3).push(q);
			}
		} else {
			click_chatter("Found an Illegal RSVP Type:%d", mtype);
			//TODO: Report to application?
		}

	} else if (packet_ip_header->ip_p == 17) {
		//We found an UDP Packet. Check to see if RSVP is active, if it is, we put the TOS byte up.
		bool found = false;
		for (Vector<PathState>::iterator it = PsVector.begin(); it != PsVector.end(); it++) {
			if ((*it).session.dstAddress == packet_ip_header->ip_dst
			        && (*it).session.dstPort == htons(d_port)
			        && (*it).session.protocolId == packet_ip_header->ip_p) {

				//click_chatter("Found the right Session.");

				if (it->path_state_set && it->resv_state_set) {
					//click_chatter("Both states are set!");
					found = true;
				}
			}
		}

		if (m_RSVP_Sender && m_give_prio && (m_prio_port == d_port) && found) {
			//click_chatter("I'm the host, RSVP IS ON.");
			//RSVP is active, we set the tos byte to 1 to assign priority, and push it through.
			packet_ip_header->ip_tos = 1;
			packet_ip_header->ip_sum = 0;
			packet_ip_header->ip_sum = click_in_cksum((unsigned char*) packet_ip_header,
			        packet_ip_header->ip_hl << 2);
			output(1).push(q);
		} else if (m_RSVP_Sender && !m_give_prio) {
			//Temporarily kill packets until RSVP has started.
			output(2).push(q);
			//q->kill();
		} else {
			//Routers will get here, as they are not the sender when they push packets forward.
			//We should check if the path state is set, and whether it corresponds to the packets
			//If so, we should give them priority, otherwise push them further without.
			//click_chatter("[%s] %d, %d",inet_ntoa(m_localIP),this->m_path_state.session.dstPort, d_port);
			bool found = false;
			//click_chatter("%d", d_port);
			for (Vector<PathState>::iterator it = PsVector.begin(); it != PsVector.end(); it++) {
				if ((*it).session.dstAddress == packet_ip_header->ip_dst
				        && (*it).session.dstPort == htons(d_port)
				        && (*it).session.protocolId == packet_ip_header->ip_p) {

					//click_chatter("Found the right Session.");

					if (it->path_state_set && it->resv_state_set) {
						//click_chatter("Both states are set!");
						found = true;
					}
				}
			}
			if (found) {
				//click_chatter("Port is correctly set, giving prio to this packet.");
				//Our path state is alive, and we have the right stream, so give it priority.
				output(1).push(q);
			} else {
				//Packet doesn't deserve Qos, toss it out in the non prio queue.
				output(2).push(q);
			}
		}

	} else {
		click_chatter("Not an RSVP Packet or UDP/IP Packet, just pushing it through.");
		output(1).push(q);
	}
}
/*
 * http://stackoverflow.com/questions/199333/how-to-detect-integer-overflow-in-c-c
 */
size_t highestOneBitPosition(uint16_t a)
{
	size_t bits = 0;
	while (a != 0) {
		++bits;
		a >>= 1;
	};
	return bits;
}

bool addition_is_safe(uint16_t a, uint16_t b)
{
	size_t a_bits = highestOneBitPosition(a), b_bits = highestOneBitPosition(b);
	return (a_bits < 16 && b_bits < 16);
}

uint16_t RsvpChecksum(unsigned char * message, int msg_size)
{
	/*
	 * http://www.faqs.org/rfcs/rfc1071.html
	 * This RFC shows optimal way of calculating the Checksum for the RSVP Message
	 */
//msg_size in bytes.
//All packets and header are formed using 32 bits each. Therefore, we must be returned
//a value which is divisible by 4 (8*4 bits = 32)
//Checksum should be set to 0 for calculation.
//message is a pointer to the packet. We need to access all bytes.
//Checksum is the 1's complement of:
//the 1's complement sum of the message, and the checksum, set to 0 for the calculation;
	assert(msg_size % 4 == 0);
	uint32_t checksm = 0;
	uint16_t *head = reinterpret_cast<uint16_t*>(message);
	for (int i = 1; i <= msg_size / 2; i++) {
		if (addition_is_safe(checksm, (*head))) {
			//click_chatter("addition is safe");
			checksm += (*head);
		} else {
			//Find a way to make sure "overflow" has occurred.
			//click_chatter("Addition is unsafe");
			checksm += (*head);
			//checksm++;
		}

		head = head + 1; //Move up 2 bytes.
	}
	while (checksm != (uint16_t) checksm) {
		uint32_t tmp = checksm;
		uint16_t lowest = checksm & 0x0000ffff;
		uint16_t highest = checksm >> 16;
		checksm = lowest + highest;
		//click_chatter("RFC 1071 Method: %zu ||| Final Checksum:%zu", tmp, checksm);
	}
//click_chatter("Returning Checksm");
	return ~checksm; //1's complement.
}

void RsvpDaemon::run_timer(Timer* t)
{
	if (this->m_RSVP_Sender && !this->m_rsvp_started) {
		click_chatter("[%s] I'm starting RSVP!", inet_ntoa(m_localIP));
		this->InitRsvp(this->m_packet_source_address, this->m_packet_dst_address, this->m_packet_source_port,
		        this->m_packet_dst_port);
		m_periodic_path_sending = true;
		this->m_PathInit_timer.reschedule_after_sec(5); //Resend PATHS
	} else if (this->m_RSVP_Sender && this->m_periodic_path_sending) {
		click_chatter("[%s] Refreshing the Path States by sending new PATH messages according to R.",
		        inet_ntoa(m_localIP));
		this->InitRsvp(this->m_packet_source_address, this->m_packet_dst_address, this->m_packet_source_port,
		        this->m_packet_dst_port);

		m_sender_period_count++;
		click_chatter("%d", m_sender_period_count);
		if (m_sender_period_count == 2) {
				click_chatter("=======================================");
				click_chatter("After 2 Path sessions, the next timer will cause a Path Tear by the sender.");
				click_chatter("This is only done to showcase functionality and is normally not the case.");
				click_chatter("=======================================");
				m_periodic_path_sending = false;
				m_Sender_init_breakoff = true;
			//COMMENT OUT TO GENERATE PATHS
		}

		this->m_PathInit_timer.reschedule_after_msec(this->m_local_state_lifetime / (3.5 * 1.5));
		click_chatter("Sending new Path messages after: %f ms.", (this->m_local_state_lifetime / 3.5) / 1.5);

	} else if (m_RSVP_Sender && m_Sender_init_breakoff && m_rsvp_started) {
		click_chatter("[%s] Sending out a Path TEAR to break off the path states.", inet_ntoa(m_localIP));
		this->SenderInitTearDown();
		m_Sender_init_breakoff = false;
		m_rsvp_started = false;
		m_give_prio = false;
		this->m_PathInit_timer.reschedule_after_msec(20000);
	} else if (m_RSVP_Receiver && m_Receiver_ResvTear_init) {
		this->ReceiverInitTearDown(); //COMMENT OUT TO GENERATE RESVTEAR
		m_Receiver_ResvTear_init = false;
		this->m_PathInit_timer.reschedule_after_sec(20);
	} else if (m_RSVP_Receiver) {
//		m_sender_period_count++;
//		if (m_sender_period_count == 1) {
//			click_chatter("[%s] Initiating a teardown in 20 Seconds.", inet_ntoa(m_localIP));
//			click_chatter("============================================");
//			m_Receiver_ResvTear_init = true;
//			this->m_PathInit_timer.reschedule_after_msec(20000);
//		} else {
//			m_Receiver_ResvTear_init = false;
//			this->m_PathInit_timer.reschedule_after_msec(10000);
//		}
		//this->ReceiverInitTearDown();
		this->m_PathInit_timer.reschedule_after_msec(1000);
	} else if (!m_RSVP_Receiver && !m_RSVP_Sender && m_rsvp_setup_complete) {
		//The only time a timer can expire, happens here.
		click_chatter("[%s] I'm starting a teardown, because my timer expired.", inet_ntoa(m_localIP));
		this->SenderInitTearDown();
	} else {
		this->m_PathInit_timer.reschedule_after_msec(1000);

	}
}
CLICK_ENDDECLS
EXPORT_ELEMENT(RsvpDaemon)
