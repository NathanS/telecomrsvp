#ifndef OBJECTHEADER_H
#define OBJECTHEADER_H

struct ObjectHeader {
	uint16_t length; //Must be multiple of 4!
	uint8_t class_num; // See appendix A
	uint8_t c_type; // See Appendix A
	// RFC2205 Page 33
};

#endif
