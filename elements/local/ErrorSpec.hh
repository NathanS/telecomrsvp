#ifndef ERRORSPEC_H
#define ERRORSPEC_H

struct ErrorSpec {
	in_addr errorNodeAddress;
	uint8_t flags;
	uint8_t errorCode;
	uint16_t errorValue;
};

#endif
