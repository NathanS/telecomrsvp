/*
 * RouterAlertIP.hh
 *
 *  Created on: Aug 8, 2016
 *      Author: paul
 */

#ifndef ROUTERALERTIP_HH_
#define ROUTERALERTIP_HH_

struct RouterAlertIP {
	uint8_t f1 = 146;
	uint8_t f2 = 4;
	uint16_t value;
};



#endif /* ROUTERALERTIP_HH_ */
