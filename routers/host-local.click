// Output configuration: 
//
// Packets for the network are put on output 0
// Packets for the host are put on output 1
elementclass Host {
	$address, $gateway |

rsvpHost::RsvpDaemon(ADDRESS $address, PRIOPORT 2222, VERBOSE true);
TOSclass::IPClassifier(ip tos 1, -);
priosched::PrioSched();

rt :: StaticIPLookup(
			$address:ip/32 0,
			$address:ipnet 1,
			0.0.0.0/0 $gateway 1);

	// Shared IP input path
	ip :: Strip(14)
		-> c2::CheckIPHeader
		-> rt;
		
	c2 [1] -> ToDump(InvalidPackets.pcap, ENCAP IP);
	
	rt [0] -> [1]output;

	rt[1]	-> ipgw :: IPGWOptions($address)
		-> FixIPSrc($address)
		-> ttl :: DecIPTTL
		-> frag :: IPFragmenter(1500)
		-> arpq :: ARPQuerier($address)
		-> output;

	ipgw[1]	-> ICMPError($address, parameterproblem)
		-> output;

	ttl[1]	-> ICMPError($address, timeexceeded)
		-> output;

	frag[1]	-> ICMPError($address, unreachable, needfrag)
		-> output;

	// incoming packets
	input	
		-> HostEtherFilter($address)
		-> in_cl :: Classifier(12/0806 20/0001, 12/0806 20/0002, 12/0800) //ARP REQ, ARP REP, IP
		-> arp_res :: ARPResponder($address)
		-> output;

	in_cl[1]
		-> [1]arpq;

	in_cl[2]
		-> rsvpHost
		-> c1::CheckIPHeader
		-> rt;
		
	rsvpHost [1] 
		-> TOSclass;
		//Output 0 is tosbyte set, output 1 is not.
		
	c1 [1] -> ToDump(H1ChIPH1.pcap, ENCAP IP);
	
	TOSclass [0] -> q1::Queue -> [0] priosched;
	TOSclass [1] -> q2::Queue -> [1] priosched;
	
	rsvpHost [2] -> q2;
	
	rsvpHost [3] -> ip;
	
	priosched -> RatedUnqueue(RATE 1000) -> ip;
}
