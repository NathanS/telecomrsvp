//Demonstrates rudimentary Scheduling with existing Click Elements.

rs1::RatedSource(RATE 10);
//rs2::RatedSource(RATE 2);
pinger::ICMPPingSource(SRC 1.1.1.1, DST 2.2.2.2, INTERVAL 1);
q1::Queue();
q2::Queue();
//ps::PrioSched();
ps::RoundRobinSched();

rs1 -> [0] q1 -> [0] ps;
pinger -> IPEncap(PROTO 1, SRC 18.26.4.24, DST 140.247.60.147) -> [0] q2 -> [1] ps;

ps [0] -> RatedUnqueue(RATE 5) -> ToDump(result.dump);


//Using the PrioSched, priority is given to the first input, which generates so many packets that
//the second source is completely blocked, and will never be pulled from.
//When using the RoundRobin Scheduler, fairer results are achieved, where the slower source will also
//be able to transmit packets.

